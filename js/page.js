/*
  * api.js
	API for RAGE taxonomy services and RAGE asset services.
	
  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	
	========================================================
*/

pendingConnections = 0;
totalConnections = 0;

taxonomies = {};
taxonomies['development-status'] = {};
taxonomies['development-status']['http://purl.org/adms/status/Completed'] = 'Completed';
taxonomies['development-status']['http://purl.org/adms/status/Deprecated'] = 'Depricated';
taxonomies['development-status']['http://purl.org/adms/status/UnderDevelopment'] = 'Under development';
taxonomies['development-status']['http://purl.org/adms/status/Withdrawn'] = 'Withdrawn';

options = {};
(function retrieveOptions()
{
	var match,
		pl     = /\+/g,  // Regex for replacing addition symbol with a space
		search = /([^&=]+)=?([^&]*)/g,
		decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
		query  = window.location.search.substring(1);

	while (match = search.exec(query))
	   options[decode(match[1])] = decode(match[2]);
})();
if (!options.resource) options.permissions='rw'; // temporary allow edits, still there is no saving, so it is ok

var CSS_RO = '<style>*[]{} *[ro] {display:block;} *[rw] {display:none;}</style>';
var CSS_RW = '<style>*[]{} *[ro] {display:none;}  *[rw] {display:block;}  *[rwi] {display:inline-block;}</style>';

var css = document.createElement("style");
css.type = "text/css";
css.innerHTML = (options.permissions=='rw')?CSS_RW:CSS_RO;
document.head.appendChild(css);

TaxonomyJSON = {
	// JSON-LD keywords
	ID				: '@id',
	GRAPH			: '@graph',
	LANGUAGE		: '@language',
	VALUE			: '@value',
	TYPE			: '@type',
	// SKOS keywords
	PREF_LABEL		: 'http://www.w3.org/2004/02/skos/core#prefLabel',
	NARROWER		: 'http://www.w3.org/2004/02/skos/core#narrower',
	CONCEPT			: 'http://www.w3.org/2004/02/skos/core#Concept',
	CONCEPT_SCHEME	: 'http://www.w3.org/2004/02/skos/core#ConceptScheme',
	HAS_TOP_CONCEPT	: 'http://www.w3.org/2004/02/skos/core#hasTopConcept',
	// Dublin Core keywords
	TITLE			: 'http://purl.org/dc/terms/title',
}
function constructTaxonomyTree(taxonomyString)
{
	function findNodeByType(type)
	{
		return json.filter(function(o){return o[TaxonomyJSON.TYPE][0]==type})[0];
	}
	function findNodeById(id)
	{
		return json.filter(function(o){return o[TaxonomyJSON.ID]==id})[0];
	}
	function constructNode(parent, obj, level)
	{
		var node = {};

		node.id = obj[TaxonomyJSON.ID];
		node.level = level;
		//node.parent = parent;
		//node.expanded = true;
		//node.hidden = false;
		
		// extract labels in all available languages
		node.label = '';
		(obj[TaxonomyJSON.PREF_LABEL]||[]).forEach(function(elem)
		{
			if (elem[TaxonomyJSON.LANGUAGE]=='en')
				node.label = elem[TaxonomyJSON.VALUE];
		});

		// recursively construct children
		node.child = [];
		(obj[TaxonomyJSON.NARROWER]||obj[TaxonomyJSON.HAS_TOP_CONCEPT]||[]).forEach(function(elem)
		{
			elem = findNodeById(elem[TaxonomyJSON.ID]);
			node.child.push( constructNode(node,elem,level+1) );
		});
		
		return node;
	}
	var json = JSON.parse(taxonomyString);
	json = json[TaxonomyJSON.GRAPH] || json;
	var root = constructNode(null,findNodeByType(TaxonomyJSON.CONCEPT_SCHEME),0);
	return root;
}

function processTaxonomy(taxonomyName,taxonomyString)
{
	taxonomies[taxonomyName] = {};
	
	var root = constructTaxonomyTree(taxonomyString);
	var str = '';
	(function traverse(node)
	{
		if (node!=root)
			taxonomies[taxonomyName][node.id] = node.label;
		for (var i=0; i<node.child.length; i++)
			traverse(node.child[i]);
	})(root);
	//console.log('TAXONOMY',taxonomyName);
}

function showMessage(text)
{
	console.error(text);
}

function loadTaxonomy(taxonomy)
{
	pendingConnections++;
	rage.getByURI({
		themeTaxonomy: encodeURIComponent(taxonomy),
		success: function(data) {
			pendingConnections--;
			processTaxonomy(taxonomy,data)
		},
		error: function(e) {
			fatalError( 'We tried to retrieved some data from the repository, but failed. There might be a network problem or insufficient permissions.');
			pendingConnections--;
			console.error('Could not load taxonomy ',taxonomy);
		}
	});
}

function showTaxonomy(taxonomy)
{
	pendingConnections++;
	rage.getByURI({
		themeTaxonomy: encodeURIComponent(taxonomy),
		success: function(data) {
			console.log(data);
		},
		error: function(e) {pendingConnections--; console.error('Could not load taxonomy ',taxonomy)}
	});
}

function fatalError(message)
{
	document.getElementById('error_bar').style.display = 'block';
	document.getElementById('error_bar').innerHTML = message+' Sorry. m(_ _)m';
}


function showStep(step)
{
	document.getElementById('main').style.display = 'block';
	
	for (var i=1; i<=8; i++)
	{
		var elem = document.getElementById('step-'+i)
		if (elem) elem.style.display = 'none';
		var elem = document.getElementById('nav-step-'+i)
		if (elem) elem.style.backgroundColor = 'WhiteSmoke';
		if (elem) elem.style.color = 'gray';
	}
	
	var elem = document.getElementById('step-'+step);
	if (elem) elem.style.display = 'block';
	elem = document.getElementById('nav-step-'+step);
	if (elem) elem.style.backgroundColor = 'MediumSeaGreen';
	if (elem) elem.style.color = 'White';

	window.scrollTo(0, 0);
	
	// last step - 8 - the actual submission of data
	if (step==8) submitMetadata();
}

function goBack()
{
	// go back
	//console.log('go back');
	var url = options.back;
	window.location.href = decodeURIComponent(url);
}

function goToAssetManager()
{			
	var url = '../am/am.html';
	//console.log(url);
	window.location.href = url;
}

function editAsset()
{
	var resource = '?resource='+options.resource;
	var permissions = '&permissions='+options.permissions;
	var back = '&back='+encodeURIComponent('vew.html'+resource+permissions);
	
	var url = 'mew.html'+resource+permissions+back;
	//console.log(url);
	window.location.href = url;
}

function loadMetadata(resource)
{
	if (typeof SAMPLE_METADATA != 'undefined')
	{
		rage.hideLoader();
		console.warn('Using an old sample software asset metadata instead of the metadata of software asset',resource);
		readMetadata(SAMPLE_METADATA);
		hideUnusedElements();
		return;
	}
	
	rage.showLoader('Loading software asset');
	rage.getAssetMetadata({
		assetId: resource,
		success: function(data) {
			rage.hideLoader();
			originalMetadata = data;
			readMetadata(data);
			hideUnusedElements();
		},
		error: function(e) {
			rage.hideLoader();
			fatalError( 'We tried to retrieved the description of a software asset from the repository, but failed. There might be a network problem or insufficient permissions. As a result, you cannot view this software asset now.');
			console.error('Could not retrieve metadata of software asset',resource);
			hideUnusedElements();
		},
	});
}

function downloadAsset()
{
	var assetId = options.resource;
	var urlZip = rage.SERVER+'/assets/'+assetId;

	function error()
	{
		showMessage('Could not download software asset "'+items[assetId].title+'"');
	}
	
	function success(data)
	{
		window.location.href = urlZip;
		//window.location.href = data;
		console.log('download software asset',assetId,data);
	}

	rage.downloadAsset({success: success, error: error, assetId: assetId});
}

function requestCloneAsset()
{
	var inst = $('[data-remodal-id=clone-asset]').remodal();
	inst.open();
	
	document.getElementById('clone-ok').onclick = cloneAsset;
}

function cloneAsset()
{
	var assetId = options.resource;

	function error()
	{
		showMessage('Could not copy software asset "'+items[assetId].title+'"');
	}
	
	function success(data)
	{
		// convert AssetURI->AssetID
		var newResource = data.split('/assets/');
		newResource =newResource[newResource.length-1];
		
		var resource = '?resource='+newResource;
		var permissions = '&permissions='+options.permissions;
		var back = '&back='+encodeURIComponent('vew.html'+resource+permissions);
		
		var url = 'mew.html'+resource+permissions+back;
		//console.log(url);
		window.location.href = url;
	}

	rage.cloneAsset({success: success, error: error, assetId: assetId});
}

function requestDeleteAsset()
{
	var inst = $('[data-remodal-id=delete-asset]').remodal();
	inst.open();
	
	document.getElementById('delete-ok').onclick = deleteAsset;
}
		
function deleteAsset()
{
	var assetId = options.resource;

	function error()
	{
		showMessage('Could not delete software asset "'+assetId+'"');
	}
	
	function success()
	{
		goToAssetManager();
	}

	rage.deleteAsset({success: success, error: error, assetId: assetId});
}

function main()
{
	rage.showLoader('Connecting RAGE');
	//loadTaxonomy('http://rageproject.eu/taxonomy/asset-type#asset-type'); REMOVAL REQUESTED BY WIM, 2017.04.18
	loadTaxonomy('http://rageproject.eu/taxonomy/licns-type#licns-type');
	loadTaxonomy('http://rageproject.eu/taxonomy/game-engine#game-engine');
	loadTaxonomy('http://rageproject.eu/taxonomy/game-platfrm#game-platfrm');
	loadTaxonomy('http://rageproject.eu/taxonomy/progrmmng-langg#progrmmng-langg');
	
	//loadTaxonomy('http://rageproject.eu/taxonomy/game-genre#game-genre'); REMOVAL REQUESTED BY WIM, 2017.04.18
	loadTaxonomy('http://rageproject.eu/taxonomy/learnng-goals#learnng-goals');
	loadTaxonomy('http://rageproject.eu/taxonomy/knowldg-transfer#knowldg-transfer');
	loadTaxonomy('http://rageproject.eu/taxonomy/acm-ccs-applied#acm-ccs-applied');
	loadTaxonomy('http://rage.deploy2.ftk.de/taxonomy/SHOP#59bfe60d4f26c');

	totalConnections = pendingConnections;
	var ticks = 0/*, ticksStr='\u2252\u2251\u2253\u2251'*/;
	var waitPending = setInterval(function(){
		ticks++;
		/*document.getElementById('connection-bar').innerHTML = 'Connecting \u22D0'+ticksStr[ticks%4]+'\u22D1. Please wait or reload the page.';*/
		if (pendingConnections==0)
		{
			clearInterval(waitPending);
			/*document.getElementById('connection-bar').style.display = 'none';*/
			if (options.resource)
				loadMetadata(options.resource);
			else
				rage.hideLoader();
		}
		if (ticks==200)
		{
			rage.hideLoader();
			clearInterval(waitPending);
			/*document.getElementById('connection-bar').style.display = 'none';*/
			fatalError( 'We tried to retrieved some data from the repository, but failed. There might be a network problem or insufficient permissions. As a result, some aspects of this tool might not function as expected.');
			console.error('failed loading taxonomies');
		}
	},200);

	showStep(1);
}