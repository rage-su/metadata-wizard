/*
  * api.js
	API for RAGE taxonomy services and RAGE asset services.
	
  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	
	========================================================
*/
		ECHO_METADATA = !true; // if true, then ignire all changes and submit the original metadata
		originalMetadata = ''; // metadata read from the server
		
		pendingConnections = 0;
		totalConnections = 0;
		
		options = {};
		(function retrieveOptions()
		{
			var match,
				pl     = /\+/g,  // Regex for replacing addition symbol with a space
				search = /([^&=]+)=?([^&]*)/g,
				decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
				query  = window.location.search.substring(1);

			while (match = search.exec(query))
			   options[decode(match[1])] = decode(match[2]);
		})();
		if (!options.resource) options.permissions='rw'; // temporary allow edits, still there is no saving, so it is ok

		var CSS_RO = '<style>*[]{} *[ro] {display:block;} *[rw] {display:none;}</style>';
		var CSS_RW = '<style>*[]{} *[ro] {display:none;}  *[rw] {display:block;}  *[rwi] {display:inline-block;}</style>';
		
		var css = document.createElement("style");
		css.type = "text/css";
		css.innerHTML = (options.permissions=='rw')?CSS_RW:CSS_RO;
		document.head.appendChild(css);
			
		if (options.permissions=='rw')
		{
			$( function() {
				$( "#datepicker" ).datepicker({
					showOn: 'button',
					buttonImage: 'images/calendar.png',
					buttonImageOnly: true,
					dateFormat: 'yy-mm-dd',
					constrainInput: true,
					onClose: function() {$(this).trigger('blur');}
				});
				//$( "#3-input" ).selectmenu();
			});
		}

		TaxonomyJSON = {
			// JSON-LD keywords
			ID				: '@id',
			GRAPH			: '@graph',
			LANGUAGE		: '@language',
			VALUE			: '@value',
			TYPE			: '@type',
			// SKOS keywords
			PREF_LABEL		: 'http://www.w3.org/2004/02/skos/core#prefLabel',
			NARROWER		: 'http://www.w3.org/2004/02/skos/core#narrower',
			CONCEPT			: 'http://www.w3.org/2004/02/skos/core#Concept',
			CONCEPT_SCHEME	: 'http://www.w3.org/2004/02/skos/core#ConceptScheme',
			HAS_TOP_CONCEPT	: 'http://www.w3.org/2004/02/skos/core#hasTopConcept',
			// Dublin Core keywords
			TITLE			: 'http://purl.org/dc/terms/title',
		}
		function constructTaxonomyTree(taxonomyString)
		{
			function findNodeByType(type)
			{
				return json.filter(function(o){return o[TaxonomyJSON.TYPE][0]==type})[0];
			}
			function findNodeById(id)
			{
				return json.filter(function(o){return o[TaxonomyJSON.ID]==id})[0];
			}
			function constructNode(parent, obj, level)
			{
				var node = {};

				node.id = obj[TaxonomyJSON.ID];
				node.level = level;
				//node.parent = parent;
				//node.expanded = true;
				//node.hidden = false;
				
				// extract labels in all available languages
				node.label = '';
				(obj[TaxonomyJSON.PREF_LABEL]||[]).forEach(function(elem)
				{
					if (elem[TaxonomyJSON.LANGUAGE]=='en' || typeof elem[TaxonomyJSON.LANGUAGE]==='undefined')
						node.label = elem[TaxonomyJSON.VALUE];
				});

				// recursively construct children
				node.child = [];
				(obj[TaxonomyJSON.NARROWER]||obj[TaxonomyJSON.HAS_TOP_CONCEPT]||[]).forEach(function(elem)
				{
					elem = findNodeById(elem[TaxonomyJSON.ID]);
					node.child.push( constructNode(node,elem,level+1) );
				});
				
				return node;
			}
			var json = JSON.parse(taxonomyString);
			json = json[TaxonomyJSON.GRAPH] || json;
			var root = constructNode(null,findNodeByType(TaxonomyJSON.CONCEPT_SCHEME),0);
			return root;
		}
		
		function putInSelect(taxonomyString,selectId)
		{
			var root = constructTaxonomyTree(taxonomyString);
			var str = '';
			(function traverse(node)
			{
				if (node!=root)
				{
					str += '<option value="'+node.id+'">'+node.label+'</option>';
				}
				else
				{
					str += '<option value=""></option>';
				}
				for (var i=0; i<node.child.length; i++)
					traverse(node.child[i]);
			})(root);
			
			document.getElementById(selectId).innerHTML = str;
		}
		
		function putAssetsInSelect(assetsString,initialValue,selectId)
		{
			var str = '<option value="">'+initialValue+'</option>';
			var json = JSON.parse(assetsString);
			for (var i=1; i<json.length; i++) // skip index 0
			{
				var asset = json[i];
				var id = asset[TaxonomyJSON.ID];
				var name = asset[TaxonomyJSON.TITLE][0]['@value']; // pick the first name
				if (name!='Empty RAGE Component')
				{
					str += '<option value="'+name+'">'+name+'</option>';
				}
				document.getElementById(selectId).innerHTML = str;
			}
		}
		
		function putInDiv(taxonomyString,divId,listId,warningId)
		{
			var root = constructTaxonomyTree(taxonomyString);
			var str = '';
			(function traverse(node)
			{
				if (node!=root)
				{
					str += '<div id="'+node.id+'" class="taxonomy-element" style="padding-left:'+(2*node.level-1)+'em" onclick="taxonomyElementClick(this,\''+listId+'\',\''+warningId+'\')">';
					str += node.label;
					str += '</div>';
				}
				for (var i=0; i<node.child.length; i++)
					traverse(node.child[i]);
			})(root);
			document.getElementById(divId).innerHTML = str;
		}

		function keepAlive()
		{
			// used to imitate as if the client is still connected
			// to the server, so the login will not timeout
			rage.ping({
				success: function(data) {console.log('Keep alive: connection is OK.')},
				error: function(e) {console.log('Keep alive: connection is lost.')}
			});
		}
		
		function loadTaxonomy(taxonomy,func,param1,param2,param3)
		{
			pendingConnections++;
			rage.getByURI({
				themeTaxonomy: encodeURIComponent(taxonomy),
				success: function(data) {
					pendingConnections--;
					func(data,param1,param2,param3)
					validateSections();
					updateProgressBar();
				},
				error: function(e) {
					fatalError( 'We tried to retrieved some data from the repository, but failed. There might be a network problem or insufficient permissions. As a result, some aspects of this tool might not function as expected.');
					pendingConnections--;
					console.error('Could not load taxonomy ',taxonomy);
				}
			});
		}
		
		function showTaxonomy(taxonomy)
		{
			rage.getByURI({
				themeTaxonomy: encodeURIComponent(taxonomy),
				success: function(data) {
					console.log(data);
				},
				error: function(e) {pendingConnections--; console.error('Could not load taxonomy ',taxonomy)}
			});
		}

		function fatalError(message)
		{
			document.getElementById('error_bar').style.display = 'block';
			document.getElementById('error_bar').innerHTML = message+' Sorry. m(_ _)m';
		}
		
		/* NO RELATED ASSETS
		function loadAssetList(selectPrev,selectNext)
		{
			pendingConnections++
			rage.getAssetsList({
				success: function(data) {
					pendingConnections--;
					putAssetsInSelect(data,'No previous version',selectPrev);
					putAssetsInSelect(data,'No next version',selectNext);
					validateSections();
					updateProgressBar();
				},
				error: function(e) {
					pendingConnections--;
					if (typeof ASSET_LIST_SAMPLE != 'undefined')
					{
						console.warn('Using an old sample list of software assets');
						putAssetsInSelect(ASSET_LIST_SAMPLE,'No previous version',selectPrev);
						putAssetsInSelect(ASSET_LIST_SAMPLE,'No next version',selectNext);
						validateSections();
						updateProgressBar();
					}
					else
					{
						fatalError( 'We tried to retrieved the list of software assets from the repository, but failed. There might be a network problem or insufficient permissions. As a result, some aspects of this tool might not function as expected.');
						console.error('Could not load the list of software assets');
					}
				},
				limit: 1000,
				offset: 0,
			});
		}
		NO RELATED ASSETS */
		
		function consoleTaxonomyList(select)
		{
			rage.list({
				success: function(data) {
					var json = JSON.parse(data);
					var list = [];
					
					// pick only the leafs of the taxonomy
				nextConcept:	
					for (var i in json)
					{
						var taxon = {
							uri:  json[i]['http://www.w3.org/ns/dcat#themeTaxonomy'][0]['@id'],
							id:   json[i]['@id'],
							name: json[i]['http://purl.org/dc/terms/title'][0]['@value']
						};

						var title = json[i]['http://purl.org/dc/terms/title'];
						for (var t in title)
							if (title[t]['@language']=='en')
								taxon.name = title[t]['@value'];

						if (!select)
						{
							console.log(taxon.name);
							console.log(taxon.id);
							console.log(taxon.uri);
							console.log();
						}
						list.push(taxon);
					}

					var str = '';
					for (var i in list)
						str += '<option value="'+list[i].id+'">'+list[i].name+'</option>';
					if (select) document.getElementById(select).innerHTML = str;
				},
				error: function(e) {
					console.log(e)
				}
			});
		}

		function showResourceHints(button)
		{
			button.style.display = 'none';
			document.getElementById('resources-hints').style.display = 'block';
		}
		
		function showStep(step)
		{
			validateSections();
			
			for (var i=1; i<=9; i++)
			{
				var elem = document.getElementById('step-'+i);
				if (elem) elem.style.display = 'none';
				var elem = document.getElementById('nav-step-'+i)
				if (elem) elem.style.backgroundColor = 'WhiteSmoke';
				if (elem) elem.style.color = 'gray';
			}

			var elem = document.getElementById('step-'+step);
			if (elem) elem.style.display = 'block';
			elem = document.getElementById('nav-step-'+step);
			if (elem) elem.style.backgroundColor = 'MediumSeaGreen';
			if (elem) elem.style.color = 'White';

			window.scrollTo(0, 0);
			
			// last step - 9 - the actual submission of data
			if (step==9) submitMetadata();
		}

		function submitMetadata()
		{
			var UPLOAD_METADATA=1, DELETE_ARTEFACT=2, UPLOAD_ARTEFACT=3, UPLOAD_LOGO=4;
			// collect info for submitting
			var actionList = [];
			var actionCount = 0;
			//var logElem = document.getElementById('submit-log');
			var percentageElem = document.getElementById('nav-section-submit');
			percentageElem.innerHTML = '<div class="percentage">0%</div><div class="text">submitted</div>';
			
			//document.getElementById('submit-process').style.display = 'block';
			document.getElementById('submit-result').style.display = 'none';
			document.getElementById('submit-fail').style.display = 'none';
			
			function updatePercentage()
			{
				var percentage = (100-100*actionList.length/actionCount).toFixed(0);
				document.getElementById('nav-section-submit').innerHTML = '<div class="percentage">'+percentage+'%</div><div class="text">submitted</div>';
			}
			
			function submitFirstItem()
			{
				updatePercentage();
				
				// no more items
				if (actionList.length==0)
				{
					rage.hideLoader();
					console.log('sumbit done',(Date.now() % (1000*60))/1000);
					//document.getElementById('submit-process').style.display = 'none';
					document.getElementById('submit-result').style.display = 'block';
					document.getElementById('nav-bar').style.display = 'none';
					return;
				}
//console.log('UPLOAD==>');				
//console.log('\t',actionList[0]);				
				if (actionList[0].action!=DELETE_ARTEFACT)
					rage.showLoader('Uploading '+actionList[0].info);
				else
					rage.showLoader('Removing '+actionList[0].info);
				
				console.log('Start submitting '+actionList[0].info,(Date.now() % (1000*60))/1000);
				//logElem.innerHTML = 'Sending '+actionList[0].name;
				document.getElementById('submit-fail-what').innerHTML = actionList[0].name;
				
				switch (actionList[0].action)
				{
					case UPLOAD_METADATA: // submit asset metadata
							rage.uploadAssetMetadata({
								success: function() {
									//logElem.innerHTML += ' &nbsp; done.';
									console.log('submitting metadata done',(Date.now() % (1000*60))/1000);
									actionList.shift();
									updatePercentage();
									submitFirstItem();
								},
								error: function(e) {
									//logElem.innerHTML += ' &nbsp; failed.';
									//document.getElementById('submit-process').style.display = 'none';
									document.getElementById('submit-fail').style.display = 'block';
									console.error('error submitting metadata',e,(Date.now() % (1000*60))/1000);
									rage.hideLoader();
									updatePercentage();
								},
								assetId: options.resource,
								publish: privateAsset?0:1,
								metadata: xml});
								break;
								
					case UPLOAD_ARTEFACT: // submit artefact data
								rage.uploadArtefact({
									success: function() {
										//logElem.innerHTML += ' &nbsp; done.';
										console.log('submitting file',actionList[0].name,'done',(Date.now() % (1000*60))/1000);
										actionList[0].row.isNew = false; // remove NEW flag for successfully uploaded files // remove when TRANSACTIONS ARE IMPLEMENTED
										actionList[0].file = undefined; // remove file for successfully uploaded files// remove when TRANSACTIONS ARE IMPLEMENTED
										actionList.shift();
										updatePercentage();
										submitFirstItem();
									},
									error: function(e) {
										//logElem.innerHTML += ' &nbsp; failed.';
										console.error('error submitting file',e,actionList[0].name,(Date.now() % (1000*60))/1000);
										//document.getElementById('submit-process').style.display = 'none';
										document.getElementById('submit-fail').style.display = 'block';
										rage.hideLoader();
										updatePercentage();
									},
									//progress: progress,
									assetId: options.resource,
									size: actionList[0].size,
									file: actionList[0].file,
									section: actionList[0].data,
									path: actionList[0].path
								});
								break;
					case UPLOAD_LOGO: // submit artefact - logo
								rage.uploadArtefact({
									success: function() {
										//logElem.innerHTML += ' &nbsp; done.';
										console.log('submitting file',actionList[0].name,'done',(Date.now() % (1000*60))/1000);
										actionList[0].file = undefined; // remove file for successfully uploaded files// remove when TRANSACTIONS ARE IMPLEMENTED
										actionList.shift();
										updatePercentage();
										submitFirstItem();
									},
									error: function(e) {
										//logElem.innerHTML += ' &nbsp; failed.';
										console.error('error submitting file',e,actionList[0].name,(Date.now() % (1000*60))/1000);
										//document.getElementById('submit-process').style.display = 'none';
										document.getElementById('submit-fail').style.display = 'block';
										rage.hideLoader();
										updatePercentage();
									},
									//progress: progress,
									assetId: options.resource,
									size: actionList[0].size,
									file: actionList[0].file,
									section: actionList[0].data,
									path: actionList[0].path
								});
								break;
					case DELETE_ARTEFACT: // submit artefact data
								rage.deleteArtefact({
									success: function() {
										//logElem.innerHTML += ' &nbsp; done.';
										console.log('deleting file',actionList[0].name,'done',(Date.now() % (1000*60))/1000);
										if (actionList[0].row.tagName!='IMG')
										{
											actionList[0].row.parentNode.removeChild(actionList[0].row); // remove successfully deleted files// remove when TRANSACTIONS ARE IMPLEMENTED
										}
										else
										{
											//actionList[0].row.mewReference = undefined; // forget successfully deleted logo// remove when TRANSACTIONS ARE IMPLEMENTED
										}
										actionList.shift();
										updatePercentage();
										submitFirstItem();
									},
									error: function(e) {
										//logElem.innerHTML += ' &nbsp; failed.';
										console.error('error deleting file',e,actionList[0].name,(Date.now() % (1000*60))/1000);
										//document.getElementById('submit-process').style.display = 'none';
										document.getElementById('submit-fail').style.display = 'block';
										rage.hideLoader();
										updatePercentage();
									},
									//progress: progress,
									url: actionList[0].data
								});
								break;
				}
			}
			
			// asset metadata
			var xml;
			if (ECHO_METADATA) 
				xml = originalMetadata;
			else
				xml = produceMetadata();
			
			// mark metadata for upload
			actionList.push({
				action: UPLOAD_METADATA,
				name: '<b>the component description</b>',
				info: 'component',
				size: xml.length,
				data: xml });
				
			// decide what to do with each artefact
			//	г===============================T==========================¬
			//	¦ TYPE OF FILE IN MEW INTERFACE ¦ ACTION                   ¦
			//	¦=========T===========T=========+========T========T========¦
			//	¦ OLD/NEW ¦ FILE/LINK ¦ DELETED ¦ IN XML ¦ DELETE ¦ UPLOAD ¦ prod dele upld
			//	¦=========+===========+=========+========+========+========¦  --  ---- ----
			//	¦   old   ¦   file    ¦         ¦  yes   ¦        ¦        ¦  ok   ok   ok
			//	¦   old   ¦   file    ¦ deleted ¦  yes   ¦  yes   ¦        ¦  ok   ok   ok
			//	¦   new   ¦   file    ¦         ¦        ¦        ¦  yes   ¦  ok   ok   ok
			//	¦   new   ¦   file    ¦ deleted ¦        ¦        ¦        ¦  ok   ok   ok
			//	¦=========+===========+=========+========+========+========¦ ---- ---- ----
			//	¦   old   ¦   link    ¦         ¦  yes   ¦        ¦        ¦  ok   ok   ok
			//	¦   old   ¦   link    ¦ deleted ¦  yes   ¦  yes   ¦        ¦  ok   ok   ok
			//	¦   new   ¦   link    ¦         ¦  yes   ¦        ¦        ¦  ok   ok   ok
			//	¦   new   ¦   link    ¦ deleted ¦        ¦        ¦        ¦  ok   ok   ok
			//	L=========¦===========¦=========¦========¦========¦========- ---- ---- ----

			// mark files for delete
			var files = document.getElementsByClassName('artefact-filename');
			for (var i=0; i<files.length; i++)
			{
				var row = files[i];
				while (row.tagName.toUpperCase()!='TABLE') row = row.parentNode; // go up to <table> tag
				if (!row.isDeleted || row.isNew || !row.isReal) continue;

				actionList.push({
					row: row,
					action: DELETE_ARTEFACT,
					name: 'the file <b>'+files[i].innerHTML+'</b>',
					info: files[i].innerHTML,
					data: row.mewAttrURL});
			}

			// mark a new logo (if any)
			var logo = document.getElementById('43-input');
			if (logo.mewLogo)
			{
				// delete any previous logo
				if (logo.mewReference)
				{
					actionList.push({
						row: logo,
						action: DELETE_ARTEFACT,
						name: 'the file <b>'+logo.mewName+'</b>',
						info: logo.mewName,
						data: logo.mewAttrURL});
				}
				
				actionList.push({
					action: UPLOAD_LOGO,
					name: 'the file <b>'+logo.mewLogo.name+'</b>',
					info: logo.mewLogo.name,
					size: logo.mewLogo.size,
					data: 'requirements',
					file: logo.mewLogo,
					path: '_rage_logo_'});
			}
			
			// mark files for upload
			for (var i=0; i<files.length; i++)
			{
				var row = files[i];
				while (row.tagName.toUpperCase()!='TABLE') row = row.parentNode; // go up to <table> tag
				if (!row.isNew || !row.isFile || row.isDeleted ) continue;
				
				// get the path
				var match = files[i].innerHTML.trim().match(/(.*)[\/\\]/);
				var path = (match?match[1]:'')||row.path;
				if (path.length && path[0]=='/') path = path.substr(1);
				
				var file = files[i].file;
				if (!file) continue;
				
				actionList.push({
					row: row,
					action: UPLOAD_ARTEFACT,
					name: 'the file <b>'+file.name+'</b>',
					info: file.name,
					data: files[i].section,
					size: file.size,
					file: file,
					path: path});
			}

			actionCount = actionList.length;

			//console.table(actionList);
			//console.log('start submitting',(Date.now() % (1000*60))/1000);
			
			submitFirstItem();
		}
		
		//	http://stackoverflow.com/questions/1009808/enter-key-press-behaves-like-a-tab-in-javascript	
		document.addEventListener('keydown', function (event) {
			if (event.keyCode == 13)
				if (event.target.nodeName == 'INPUT' /*|| event.target.nodeName == 'TEXTAREA'*/) {
				var form = event.target.form;
				var index = Array.prototype.indexOf.call(form, event.target);
				form.elements[index].blur();
				if (form.elements[index + 1]) form.elements[index + 1].focus();
				event.preventDefault();
			}
		});

		function taxonomyElementClick(elem,listId,warningId)
		{
			//console.log('elem =',elem);
			//console.log('listId =',listId);
			//console.log('warningId =',warningId);
			
			var list = document.getElementById(listId);
			//var text = '<span class="taxonomy-selected-concept">'+elem.innerHTML+'</span>, ';
			var text = '<span>'+elem.innerHTML+'</span>, ';
			if (elem.getAttribute('checked')=='true')
			{
				elem.setAttribute('checked','false');
				list.innerHTML = list.innerHTML.replace(text,'');
			}
			else
			{
				elem.setAttribute('checked','true');
				list.innerHTML += text;
			}
			validateTaxonomySelection(list,warningId);
			validateSections();
		}

		function hideTaxonomyContainer(id)
		{
			document.getElementById(id).style.display = 'none';
			document.getElementById(id+'-button').style.display = 'inline';
		}
		
		function showTaxonomyContainer(id)
		{
			var style = document.getElementById(id).style;
			if (style.display == 'block')
			{
				style.display = 'none';
				document.getElementById(id+'-button').style.display = 'inline';
			}
			else
			{
				style.display = 'block';
				document.getElementById(id+'-button').style.display = 'none';
			}
		}
		
		function addContact(hostId,templateId,overwrite)
		{
			var host = document.getElementById(hostId);
			if (overwrite)
			{
				var clone = host.children[0];
			}
			else
			{
				var template = document.getElementById(templateId);
				var clone = template.cloneNode(true);
				clone.removeAttribute('id');
				clone.style.display = "block";
				host.appendChild( clone );
				updateProgressBar();
			}
			return clone;
		}
				
		function appendContact(hostId,sourceId)
		{
			var host = document.getElementById(hostId);
			var source = document.getElementById(sourceId);
			
			for (var i=0; i<source.children.length; i++)
			{
				var clone = source.children[i].cloneNode(true);
				clone.getElementsByClassName('contact-hidden')[0].style.display = 'block';
				host.appendChild( clone );
			}
			updateProgressBar();
		}
		
		function removeContact(elem)
		{
			while (elem.tagName.toUpperCase()!='TABLE') elem = elem.parentNode; // go up to <table> tag
			elem.parentNode.removeChild(elem);
			validateSections();
			updateProgressBar();
		}
		
		function formatSize(bytes)
		{	//http://stackoverflow.com/questions/15900485/correct-way-to-convert-size-in-bytes-to-kb-mb-gb-in-javascript
			if(bytes == 0) return '0 Bytes';
			var k = 1000,
				sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'],
				i = Math.floor(Math.log(bytes) / Math.log(k));
			return parseFloat((bytes / Math.pow(k, i)).toFixed(1)) + ' ' + sizes[i];
		}
		
		function generateUUID()
		{
			return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
				var r = Math.random()*16|0;
				var v = c == 'x' ? r : (r&0x3|0x8);
				return v.toString(16);
			});
		}

		function updateLogo(imageId,warningId)
		{
			var button = document.getElementById('file-select')
			button.onchange = null;
			button.value = '';
			button.onchange = function (event)
			{
				var files = event.target.files;
				for (var i=0; i<files.length; i++)
				{
					var name = files[i].name;
					//clone.setAttribute('id','file-'+Math.floor(Math.random()*100000));
					//clone.getElementsByClassName('artefact-filename')[0].innerHTML = folder+name;
					//clone.getElementsByClassName('artefact-filename')[0].file = files[i];
					//clone.getElementsByClassName('artefact-filename')[0].newFile = true; //generateUUID();
					//clone.getElementsByClassName('artefact-filename')[0].section = section;
					//clone.getElementsByClassName('artefact-date')[0].innerHTML = date.getFullYear()+'-'+("A0"+(date.getMonth()+1)).slice(-2)+'-'+("A0"+date.getDate()).slice(-2);
					//clone.getElementsByClassName('artefact-size')[0].innerHTML = formatSize(files[i].size);

					var zips = ['JPG','JPEG','PNG','GIF'];
					var type = name.split('.');
					if (type.length>1)
					{
						type = type[type.length-1].toUpperCase();
						if (zips.indexOf(type)<0) return; // not an image file (has unsupported extension)
					}
					else
						return; // not an image file (has no extension)
					
					// http://stackoverflow.com/questions/17138244/how-to-display-selected-image-without-sending-data-to-server
					var fr=new FileReader();
					fr.onload = function(e) { document.getElementById(imageId).src = this.result; };
					fr.readAsDataURL(files[0]);
					document.getElementById(imageId).mewLogo = files[0];
					console.log(files[0]);
					break; // process only the first file
				}
				validateSections();
				updateProgressBar();
			}
			
			button.click();
		}
		
		function addFile(hostId,templateId,section,style)
		{
			var host = document.getElementById(hostId);
			var template = document.getElementById(templateId);
			/*var folder = document.getElementById('33-input').value.trim(); REMOVAL REQUESTED BY WIM, 2017.04.18*/
			var folder = '';
			if (style=='snapshot') folder='_rage_snapshot_';
			if (folder && folder[folder.length-1]!='/') folder += '/';
			
			var button = document.getElementById('file-select');
			button.onchange = null;
			button.value = '';
			button.onchange = function (event)
			{
				var files = event.target.files;
				for (var i=0; i<files.length; i++)
				{
					var name = files[i].name;
					var date = files[i].lastModifiedDate;
					var format = files[i].type;
					
					var clone = template.cloneNode(true);
					clone.isReal = true;
					clone.isNew = true;
					clone.isFile = true;
					clone.isDeleted = false;
					clone.setAttribute('id','file-'+Math.floor(Math.random()*100000));
					clone.style.display = "block";
					clone.path = folder;
					clone.getElementsByClassName('artefact-filename')[0].innerHTML = /*folder+*/name;
					clone.getElementsByClassName('artefact-filename')[0].file = files[i];
					clone.getElementsByClassName('artefact-filename')[0].newFile = true; //generateUUID();
					clone.getElementsByClassName('artefact-filename')[0].section = section;
					clone.getElementsByClassName('artefact-date')[0].innerHTML = date.getFullYear()+'-'+("A0"+(date.getMonth()+1)).slice(-2)+'-'+("A0"+date.getDate()).slice(-2);
					clone.getElementsByClassName('artefact-size')[0].innerHTML = formatSize(files[i].size);

					if (format)
					{
						clone.getElementsByClassName('artefact-format')[0].innerHTML = format;
					}
					else
					{
						clone.getElementsByClassName('artefact-format-section')[0].style.display = "none";
					}
					var zips = ['ZIP','RAR','7Z','GZ'];
					var type = name.split('.');
					if (type.length>1)
					{
						type = type[type.length-1].toUpperCase();
						if (zips.indexOf(type)>=0)
						{
							clone.getElementsByClassName('main-icon')[0].src = 'images/files.png';
						}
					}
					host.appendChild( clone );
				}
				validateSections();
				updateProgressBar();
			}
			
			button.click();
		}
		
		function addFileNoSelect(hostId,templateId)
		{
			var host = document.getElementById(hostId);
			var template = document.getElementById(templateId);
console.log('host=',hostId);
console.log('temp=',templateId);
					
			var clone = template.cloneNode(true);
			clone.setAttribute('id','file-'+Math.floor(Math.random()*100000));
			clone.style.display = "block";
			clone.isReal = true;
			clone.isNew = true;
			clone.isFile = true;
			clone.isDeleted = false;
			host.appendChild( clone );
			validateSections();
			updateProgressBar();
			return clone;
		}
		
		function addLink(hostId,templateId,style)
		{
			var host = document.getElementById(hostId);
			var template = document.getElementById(templateId);
			
			var clone = template.cloneNode(true);
			clone.setAttribute('id','link-'+Math.floor(Math.random()*100000));
			clone.style.display = "block";
			clone.isReal = true;
			clone.isNew = true;
			clone.isFile = false;
			clone.isDeleted = false;
			clone.path = (style=='youtube')?'_rage_youtube_':'';
			host.appendChild( clone );
			validateSections();
			updateProgressBar();
			return clone;
		}
		
		function removeFile(elem)
		{
			var divElem = elem;
			while (elem.tagName.toUpperCase()!='TABLE') elem = elem.parentNode; // go up to <table> tag
			
			//document.getElementById('file-to-remove').innerHTML = name;
			//var inst = $('[data-remodal-id=file-remove]').remodal();
			//inst.open();

			if (elem.isDeleted)
			{
				elem.isDeleted = false;
				elem.setAttribute('deleted','false');
				divElem.innerHTML = '<img src="images/trash-bin.png"><br>mark for removal';
			}
			else
			{
				elem.isDeleted = true;
				elem.setAttribute('deleted','true');
				divElem.innerHTML = '<img src="images/trash-bin-undo.png"><br>undo removal';
			}
			
			//document.getElementById('file-remove-ok').onclick = function () {
			//	// remove the file
			//	elem.parentNode.removeChild(elem);
			validateSections();
			updateProgressBar();
		}
		
		function removeLink(elem)
		{
			var divElem = elem;
			while (elem.tagName.toUpperCase()!='TABLE') elem = elem.parentNode; // go up to <table> tag

			if (elem.getAttribute('deleted')=='true')
			{
				elem.isDeleted = false;
				elem.setAttribute('deleted','false');
				divElem.innerHTML = '<img src="images/trash-bin.png"><br>mark for removal';
			}
			else
			{
				elem.isDeleted = true;
				elem.setAttribute('deleted','true');
				divElem.innerHTML = '<img src="images/trash-bin-undo.png"><br>undo removal';
			}

			//elem.parentNode.removeChild(elem);
			//validateSections();
			//updateProgressBar();
		}
		
		function goBack()
		{
			// go back
			//console.log('go back');
			window.removeEventListener("beforeunload",confirmExit);
			var url = options.back?decodeURIComponent(options.back):('vew.html?resource='+options.resource+'&permissions='+options.permissions);
			window.location.href = (url);
		}
		
		function confirmCancel()
		{
			var inst = $('[data-remodal-id=cancel-submit]').remodal();
			inst.open();
			
			document.getElementById('cancel-submit-ok').onclick = goBack;
		}

		function confirmExit(e)
		{
			var msg = 'The wizard will be closed. If there are unsubmitted data, they will be lost.';
			(e || window.event).returnValue = msg;
			return msg;
		}

		function disabledSubmit()
		{
			var inst = $('[data-remodal-id=submit-disabled]').remodal();
			inst.open();
		}
		
		function loadMetadata(resource)
		{
			if (typeof SAMPLE_METADATA != 'undefined')
			{
				rage.hideLoader();
				console.warn('Using an old sample component metadata instead of the metadata of component',resource);
				readMetadata(SAMPLE_METADATA);
				return;
			}
			
			rage.showLoader('Loading component');
			rage.getAssetMetadata({
				assetId: resource,
				success: function(data) {
					rage.hideLoader();
					originalMetadata = data;
					readMetadata(data);
				},
				error: function(e) {
					rage.hideLoader();
					fatalError( 'We tried to retrieved the description of a component from the repository, but failed. There might be a network problem or insufficient permissions. As a result, you cannot edit this component now.');
					console.error('Could not retrieve metadata of component',resource);
				},
			});
		}
		
		function saveXML()
		{
			var xml
			if (ECHO_METADATA) 
				xml = originalMetadata;
			else
				xml = produceMetadata();
			var clock = new Date();
			var blob = new Blob([xml], {type: "text/plain;charset=utf-8"});
			saveAs(blob, "software_component_description_"+clock.getTime()+".xml");
		}
		
		function showXML()
		{
			var xml
			if (ECHO_METADATA) 
				xml = originalMetadata;
			else
				xml = produceMetadata();
			xml = xml.split('<').join('&lt;');
			xml = xml.split('>').join('&gt;');
			xml = xml.split('\t').join('    ');
			x=window.open();
			x.document.open();
			x.document.write('<!DOCTYPE html><head><meta charset="utf-8"/></head><body><pre>'+xml+'</pre></body>');
			x.document.close();
		}
		
		function showOriginalXML()
		{
			var xml = originalXML;
			xml = xml.split('<').join('&lt;');
			xml = xml.split('>').join('&gt;');
			xml = xml.split('\t').join('    ');
			x=window.open();
			x.document.open();
			x.document.write('<!DOCTYPE html><head><meta charset="utf-8"/></head><body><pre>'+xml+'</pre></body>');
			x.document.close();
		}
		
		function toggleElement(elem)
		{
			if (elem.getAttribute('checked')=='true')
			{
				elem.getElementsByTagName('img')[0].src = 'images/button-unchecked.png';
				elem.setAttribute('checked','false');
			}
			else
			{
				elem.getElementsByTagName('img')[0].src = 'images/button-checked.png';
				elem.setAttribute('checked','true');
			}
		}
		
		function toggleSoftwareTesting(elem)
		{
			toggleElement(elem);
			validateSoftwareTesting(elem)
		}
		
		function togglePublishing(elem)
		{
			var e = document.getElementById('38.pub.1-input');
			e.getElementsByTagName('img')[0].src = 'images/button-radio-unchecked.png';
			e.setAttribute('checked','false');
			
			var e = document.getElementById('38.pub.2-input');
			e.getElementsByTagName('img')[0].src = 'images/button-radio-unchecked.png';
			e.setAttribute('checked','false');
			
			elem.getElementsByTagName('img')[0].src = 'images/button-radio-checked.png';
			elem.setAttribute('checked','true');
		}
		
		function toggleSelfDeclaration(elem)
		{
			toggleElement(elem);
			validateSelfDeclaration(elem)
		}
		
		function main()
		{
			if (options.back)
			{
				if (options.permissions=='rw')
				{
					console.log('ye');
					var elems = document.getElementsByClassName('cancel-button');
					for (var i=0; i<elems.length; i++) elems[i].style.display = 'block';
				}
				else
				{
					console.log('nye');
					var elems = document.getElementsByClassName('back-button');
					for (var i=0; i<elems.length; i++) elems[i].style.display = 'block';
				}
			}

			if (options.permissions!='rw')
			{
				var elems = document.getElementsByTagName('input');
				for (var i=0; i<elems.length; i++)
					elems[i].setAttribute('readonly','');

				var elems = document.getElementsByTagName('textarea');
				for (var i=0; i<elems.length; i++)
					elems[i].setAttribute('readonly','');

				var elems = document.getElementsByTagName('select');
				for (var i=0; i<elems.length; i++)
					elems[i].setAttribute('disabled','');
			}
			
			if ('acem-rypy' in options)
			{
				document.getElementById('acem-rypy-section').style.display = 'block';
			}
		
		
			rage.showLoader('Connecting RAGE');
			//loadTaxonomy('http://rageproject.eu/taxonomy/asset-type#asset-type',putInSelect,'3-input'); REMOVAL REQUESTED BY WIM, 2017.04.18
			loadTaxonomy('http://rageproject.eu/taxonomy/licns-type#licns-type',putInSelect,'17-input');
			loadTaxonomy('http://rageproject.eu/taxonomy/game-engine#game-engine',putInSelect,'21-input');
			loadTaxonomy('http://rageproject.eu/taxonomy/game-platfrm#game-platfrm',putInSelect,'22-input');
			loadTaxonomy('http://rageproject.eu/taxonomy/progrmmng-langg#progrmmng-langg',putInSelect,'23-input');
			
			//loadTaxonomy('http://rageproject.eu/taxonomy/game-genre#game-genre',putInDiv,'taxonomy-gage','taxonomy-selected-concepts-gage','7-warning'); REMOVAL REQUESTED BY WIM, 2017.04.18
			loadTaxonomy('http://rageproject.eu/taxonomy/learnng-goals#learnng-goals',putInDiv,'taxonomy-lego','taxonomy-selected-concepts-lego','14-warning');
			//loadTaxonomy('http://rageproject.eu/taxonomy/knowldg-transfer#knowldg-transfer',putInDiv,'taxonomy-kntr','taxonomy-selected-concepts-kntr','15-warning'); REMOVAL REQUESTED BY WIM, 2017.04.18
			loadTaxonomy('http://rageproject.eu/taxonomy/acm-ccs-applied#acm-ccs-applied',putInDiv,'taxonomy-acac','taxonomy-selected-concepts-acac','16-warning');
			loadTaxonomy('http://rage.deploy2.ftk.de/taxonomy/SHOP#59bfe60d4f26c',putInSelect,'14shop-input');
			/* NO RELATED ASSETS
			loadAssetList('24-input','25-input');
			NO RELATED ASSETS */
			
			totalConnections = pendingConnections;
			var ticks = 0/*, ticksStr='\u2252\u2251\u2253\u2251'*/;
			var waitPending = setInterval(function(){
				ticks++;
				/*document.getElementById('connection-bar').innerHTML = 'Connecting \u22D0'+ticksStr[ticks%4]+'\u22D1. Please wait or reload the page.';*/
				if (pendingConnections==0)
				{
					clearInterval(waitPending);
					/*document.getElementById('connection-bar').style.display = 'none';*/
					if (options.resource)
						loadMetadata(options.resource);
					else
						rage.hideLoader();
				}
				if (ticks==200)
				{
					rage.hideLoader();
					clearInterval(waitPending);
					/*document.getElementById('connection-bar').style.display = 'none';*/
					fatalError( 'We tried to retrieved some data from the repository, but failed. There might be a network problem or insufficient permissions. As a result, some aspects of this tool might not function as expected.');
					console.error('failed loading taxonomies');
				}
			},200);

			window.addEventListener("beforeunload",confirmExit);

			setInterval(keepAlive,4*60*1000); // 4 minutes
			//consoleTaxonomyList();
			//showTaxonomy('http://rageproject.eu/taxonomy/licns-type#licns-type');
			//showTaxonomy('http://rage.deploy2.ftk.de/taxonomy/SHOP#59bfe60d4f26c');
			showStep(1);
			//showStep(6);
		}
