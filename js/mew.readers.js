﻿
var compat_list;

var sysMetadataVersion = 0;

function desafe(x)
{
	x = x.trim();
	x = x.split('%26').join('&');
	x = x.split('&lt;').join('<');
	x = x.split('&gt;').join('>');
	x = x.split('&quot;').join('"');
	x = x.split('%3D').join('=');
	x = x.split('%25').join('%'); // must be last

	return x;
}

//https://stackoverflow.com/questions/36810940/array-from-on-the-internet-explorer
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError("Array.from requires an array-like object - not null or undefined");
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

//https://stackoverflow.com/questions/24143604/array-prototype-find-is-undefined
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(predicate) {
      if (this == null) {
        throw new TypeError('Array.prototype.find called on null or undefined');
      }
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }
      var list = Object(this);
      var length = list.length >>> 0;
      var thisArg = arguments[1];
      var value;

      for (var i = 0; i < length; i++) {
        if (i in list) {
          value = list[i];
          if (predicate.call(thisArg, value, i, list)) {
            return value;
          }
        }
      }
      return undefined;
    }
  });
}

function findFirstElementChild(node)
{	// https://github.com/w3c/respec/commit/64c43aa583bcb222b858c8b41b6e9d516935c8df

	if (!node.hasChildNodes())
		return null;

	if (node.firstElementChild)
		return node.firstElementChild;
		
	if (node.childNodes[0].nodeType === Node.ELEMENT_NODE)
		return node.childNodes[0];
		
	return Array
		.from(node.childNodes)
		.find(function(node){
			return node.nodeType === Node.ELEMENT_NODE;
		});
}
 
 
function readMetadata(metadata)
{
	originalXML = metadata;
	
	var meta = (new DOMParser()).parseFromString(metadata,'text/xml');
	//console.dir(meta);

	compat_list = [];
	
	var asset = findFirstElementChild(meta);
	//console.dir(asset);
	if (asset.localName!='Asset') console.error('Could not locate the component, found "'+asset.localName+'" instead');
	readAsset( asset );
	
	if (compat_list.length && ('acem-rypy' in options))
	{
		document.getElementById('compatibility_button').style.display = 'block';
	}
	
	validateSections();
	updateProgressBar();
}

/* REMOVE REQUESTED BY WIM
function addCompatList(str)
{
	if (compat_list.indexOf(str)<0)
		compat_list.push(str);
}

function showCompatDetails()
{
	var str = "<p>There are compatibility issues in the software asset. Most likely it uses an outdated or updated software asset model. Occationally, there might be a bug in the programming code. Here are the issues:</p><ul>";
	for (var i=0; i<compat_list.length; i++)
		str += '<li>'+compat_list[i]+'</li>';
	str += '</ul>';
	str += '<p>What to do now?</p><p>If the issues are about ignored stuff, then you may, in turn, ignore them &ndash; they just indicate that some potentially unused data will be lost.</p>';
	str += '<p>If, however, the issues are about unsupported things, then the programming code must be fixed to either process them, or explicitly ignore them. Please, let us know about this.</p>';
	document.getElementById('compatibility_button').onclick = null;
	document.getElementById('compatibility_button').innerHTML = str;
}*/

var assetAttr;

function readAsset( asset )
{
	// scan asset attributes
	assetAttr = {};
	for (var i=0; i<asset.attributes.length; i++)
	{
		var attr = asset.attributes[i];
		assetAttr[attr.name] = attr.nodeValue;
	}
	
	// scan asset elements
	for (var elem=findFirstElementChild(asset); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'title': readAssetTitle(elem); break;
			case 'description': readAssetDescription(elem); break;
			case 'type':
				/*addCompatList('Ignored software asset type');*/ break;
				//readAssetType(elem); break; REMOVAL REQUESTED BY WIM, 2017.04.18
			case 'date': readAssetDate(elem); break;
			case 'language': readAssetLanguage(elem); break;
			case 'creator': readAssetCreator(elem,'creators',false); break;
			case 'publisher':
				/*addCompatList('Ignored software asset publisher');*/ break;
				//readAssetCreator(elem,'publishers'); break; REMOVAL REQUESTED BY WIM, 2017.04.18
			case 'owner': readAssetCreator(elem,'owners',true); break;
			case 'keyword': readAssetKeywords(elem); break;
			case 'versionInfo': readAssetVersionInfo(elem); break;
			case 'versionNotes': readAssetVersionNotes(elem); break;
			case 'status': readAssetStatus(elem); break;
			case 'maturityLevel':
				/*addCompatList('Ignored software asset maturity'); break;*/
				//readAssetMaturityLevel(elem); break; REMOVAL REQUESTED BY WIM, 2017.04.18
				readAssetMaturityLevelPublish(elem); break; //ADD REQUESTED BY DOMINIQ, 2017.11.26
			case 'accessURL': readAssetAccessURL(elem); break;
			case 'Classification': readAssetClassification(elem); break;
			case 'Solution': readAssetSolution(elem); break;
			case 'Usage': readArtefactSection(elem,'Usage'); break;
			case 'License': readAssetLicense(elem); break;
			case 'CustomMetadata': readAssetCustomMetadata(elem); break;
			case 'RelatedAsset': /*NO RELATED ASSETS readRelatedAsset(elem); NO RELATED ASSETS*/ /*addCompatList('Ignored related software asset');*/ break;
			default: {
				/*addCompatList('Unknown software asset tags');*/
				console.error('Unknown and unsupported tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
}


function readAssetTitle( dom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var inEnglish = dom.getAttribute('xml:lang')=='en';
	var target = document.getElementById('1-input');
	
	if (inEnglish || !target.value)
		target.value = value;
}


function readAssetDescription( dom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var inEnglish = dom.getAttribute('xml:lang')=='en';
	var target = document.getElementById('2-input');
	
	if (inEnglish || !target.value)
		target.value = value;
}


function readAssetLanguage( dom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var inEnglish = dom.getAttribute('xml:lang')=='en';
	var target = document.getElementById('5-input');
	
	if (inEnglish || !target.value)
		target.value = value.trim();
		
	if (target.value=='')
		target.value = 'English';	// REQUESTED BY WIM, 2017.04.18
}


function readAssetDate( dom )
{
	var value = dom.innerHTML||dom.textContent;
	var target = document.getElementById('datepicker');
	
	target.value = value.trim();
}


function readAssetVersionInfo( dom )
{
	var value = dom.innerHTML||dom.textContent;
	var target = document.getElementById('9-input');
	
	target.value = value;
}


function readAssetVersionNotes( dom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var inEnglish = dom.getAttribute('xml:lang')=='en';
	var target = document.getElementById('10-input');
	
	if (inEnglish || !target.value)
		target.value = value;
}


function readAssetCustomMetadata( data )
{
	// ignore attributes
		
	// scan elements
	var name,value;
	for (var elem=findFirstElementChild(data); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'name': name = desafe(elem.innerHTML||elem.textContent); break;
			case 'value': value = desafe(elem.innerHTML||elem.textContent); break;
			default: {
				/*addCompatList('Unknown custom metadata tags');*/
				console.error('Unknown and unsupported custom metadata tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
	
	switch (name)
	{
		case '%sys%asset completeness': // ignore
		case '%sys%asset quality': // ignore
			break;
		case '%sys%metadata version':
			sysMetadataVersion = parseInt(value)+1;
			break;
		case '%sys%coding style':
			document.getElementById('35-input').value = value;
			break;
		case '%sys%architectural conformance':
			document.getElementById('36-input').value = value;
			break;
		case '%sys%commit uri':
			document.getElementById('40-input').value = value;
			break;
		case '%sys%promotional description':
			document.getElementById('41-input').value = value;
			break;
		case '%sys%technical description':
			document.getElementById('44-input').value = value;
			break;
		case '%sys%detailed description':
			document.getElementById('42-input').value = value;
			break;
		/* REMOVAL REQUESTED BY WIM, 2017.04.18
		case '%sys%interoperability conformance':
			document.getElementById('37-input').value = value;
			break;
		*/
		case '%sys%software testing':
			for (var i=0; i<5; i++)
				if ((value[i]||0)=='1')
					toggleSoftwareTesting(document.getElementById('38.'+(i+1)+'-input'));
			break;
		case '%sys%self declaration':
			for (var i=0; i<2; i++)
				if ((value[i]||0)=='1')
					toggleSelfDeclaration(document.getElementById('39.'+(i+1)+'-input'));
			break;
		default:
			console.error('Unsupported custom metadata name '+name);
			/*addCompatList('Unknown custom metadata tag name '+name);*/
			/*
			var target = document.getElementById('13-input');
			if (target.value) target.value += ', '; 
			target.value += name+' = '+value;
			*/
	}
}


function readAssetStatus( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('11-input');
	
	target.value = value;
}


/* REMOVAL REQUESTED BY WIM, 2017.04.18
function readAssetMaturityLevel( dom )
{
	var value = dom.innerHTML;
	var target = document.getElementById('12-input');
	
	target.value = value.trim();
}
*/

/* ADDING REQUESTED BY DOMINIQ, 2017.11.26 */
function readAssetMaturityLevelPublish( dom )
{
	var e = document.getElementById('38.pub.1-input');
	e.getElementsByTagName('img')[0].src = 'images/button-radio-unchecked.png';
	e.setAttribute('checked','false');
	
	var e = document.getElementById('38.pub.2-input');
	e.getElementsByTagName('img')[0].src = 'images/button-radio-unchecked.png';
	e.setAttribute('checked','false');

	var value = dom.innerHTML;

	var elem;
	if (value=='' || value=='1')
		elem = document.getElementById('38.pub.2-input')
	else
		elem = document.getElementById('38.pub.1-input')

	elem.getElementsByTagName('img')[0].src = 'images/button-radio-checked.png';
	elem.setAttribute('checked','true');
}


/* REMOVAL REQUESTED BY WIM, 2017.04.18
function readAssetType( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('3-input');
	target.value = value;
	if (target.value!=value)
	{
		addCompatList('Could not write value to software asset type');
		console.error('Could not write value to software asset type',value);
	}
}
*/

function readAssetKeywords( dom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var target = document.getElementById('8-input');
	
	if (target.value) target.value += ', ';
	target.value += value;
}


function readAssetAccessURL( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('6-input');
	
	target.value = value;
}


function readAssetCreator( dom, id, overwrite )
{
	var contact = findFirstElementChild(dom);
	
	// scan attributes
	//var attribs = {};
	//for (var i=0; i<contact.attributes.length; i++)
	//{
	//	var attr = contact.attributes[i];
	//	attribs[attr.name] = attr.nodeValue;
	//}
	
	var contactDom = addContact(id,'contact-individual-template',overwrite);
	/*
	// contact type
	var type = contact.localName; //Person or Organization
	//console.log('!',type);
	switch (type)
	{
		case 'Person':
			var contactDom = addContact(id,'contact-individual-template');
			break;
		case 'Organizaton': // in foaf.xsd and in metadata it is Organizaton
		case 'Organization':
			var contactDom = addContact(id,'contact-organization-template');
			break;
		default:
		{
			addCompatList('Unknown contact type');
			console.error('Unknown software asset contact type "'+type+'".');
		}
	}
	*/
	
	if (contactDom)
	{
		// store attributes in the original DOM element
		//contactDom.mewPersist = attribs;
		
		// contact elements
		for (var elem=findFirstElementChild(contact); elem; elem=elem.nextElementSibling)
		{
			//console.log('>>',elem.localName);
			switch (elem.localName)
			{
				case 'name': readContactName(elem,contactDom); break;
				case 'homepage': readContactHomePage(elem,contactDom); break;
				case 'mbox': readContactMailBox(elem,contactDom); break;
				default: {
					/*addCompatList('Unknown contact tag');*/
					console.error('Unknown and unsupported contact tag '+elem.localName);
					console.dir(elem);
				}
			}
		}
	}
}


function readContactName( dom, targetDom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var target = targetDom.getElementsByTagName('input')[0];
	
	target.value = value;
}


function readContactHomePage( dom, targetDom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = targetDom.getElementsByTagName('input')[1];
	target.value = value;
}


function readContactMailBox( dom, targetDom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = targetDom.getElementsByTagName('input')[2];

	if (value.indexOf('mailto:')==0)
	{
		value = value.substring(7);
	}
	target.value = value;
}


function readAssetLicense( license )
{
	// scan attributes
	//for (var i=0; i<license.attributes.length; i++)
	//{
	//	var attr = license.attributes[i];
	//	persist.licenseAttr[attr.name] = attr.nodeValue;
	//}
	
	// scan elements
	for (var elem=findFirstElementChild(license); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'title': /*addCompatList('Ignored license title');*/ break;
			case 'description': readLicenseDescription(elem); break;
			case 'accessURL': readLicenseAccessURL(elem); break;
			case 'type': readLicenseType(elem); break;
			default: {
				/*addCompatList('Unknown license tags');*/
				console.error('Unknown and unsupported license tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
	
	if (document.getElementById('18-input').value.trim()=='')
		changeLicenseType(document.getElementById('17-input'));
}


function readLicenseDescription( dom )
{
	var value = desafe(dom.innerHTML||dom.textContent);
	var target = document.getElementById('19-input');
	
	if (value=='MEW%N/A?WEM') value='';
	
	target.value = value;
}


function readLicenseAccessURL( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('18-input');
	
	target.value = value;
}


function readLicenseType( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('17-input');
	target.value = value;
	if (target.value!=value)
	{
		/*addCompatList('Could not write value to license type');*/
		console.error('Could not write value to license type',value);
	}
}


var classifications;
function readAssetClassification( classification )
{
	// scan attributes
	//for (var i=0; i<classification.attributes.length; i++)
	//{
	//	var attr = classification.attributes[i];
	//	persist.classificationAttr[attr.name] = attr.nodeValue;
	//}
	
	// scan elements and collect used taxonomies
	classifications = {};
	for (var elem=findFirstElementChild(classification); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'description': /*addCompatList('Ignored classification description');*/ break; // ignore silently
			case 'Context': readClassificationContext(elem); break;
			default: {
				/*addCompatList('Unknown classification tags');*/
				console.error('Unknown and unsupported classification tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
	
	// process classifications
	for (var name in classifications)
	{
		//console.log(name);
		switch (name)
		{
			/* REMOVAL REQUESTED BY WIM, 2017.04.18
			case 'http://rageproject.eu/taxonomy/game-genre#game-genre':
				for (var i=0; i<classifications[name].length; i++)
				{
					var concept = classifications[name][i];
					//console.log('concept',concept);
					var elem = document.getElementById(concept);
					if (elem)
						taxonomyElementClick(elem,'taxonomy-selected-concepts-gage','7-warning');
					else
					{
						addCompatList('Unknown taxonomy concepts');
						console.error('Unknown or unsupported taxonomy concept',concept);
					}
				}
				break;
			*/
			case 'http://rageproject.eu/taxonomy/learnng-goals#learnng-goals':
				for (var i=0; i<classifications[name].length; i++)
				{
					var concept = classifications[name][i];
					//console.log('concept',concept);
					var elem = document.getElementById(concept);
					if (elem)
						taxonomyElementClick(elem,'taxonomy-selected-concepts-lego','14-warning');
					else
					{
						/*addCompatList('Unknown taxonomy concepts');*/
						console.error('Unknown or unsupported taxonomy concept',concept);
					}
				}
				break;
			/* REMOVAL REQUESTED BY WIM, 2017.04.18	
			case 'http://rageproject.eu/taxonomy/knowldg-transfer#knowldg-transfer':
				for (var i=0; i<classifications[name].length; i++)
				{
					var concept = classifications[name][i];
					//console.log('concept',concept);
					var elem = document.getElementById(concept);
					if (elem)
						taxonomyElementClick(elem,'taxonomy-selected-concepts-kntr','15-warning');
					else
					{
						addCompatList('Unknown taxonomy concepts');
						console.error('Unknown or unsupported taxonomy concept',concept);
					}
				}
				break;
			*/
			case 'http://rageproject.eu/taxonomy/acm-ccs-applied#acm-ccs-applied':
				for (var i=0; i<classifications[name].length; i++)
				{
					var concept = classifications[name][i];
					//console.log('concept',concept);
					var elem = document.getElementById(concept);
					if (elem)
						taxonomyElementClick(elem,'taxonomy-selected-concepts-acac','16-warning');
					else
					{
						/*addCompatList('Unknown taxonomy concepts');*/
						console.error('Unknown or unsupported taxonomy concept',concept);
					}
				}
				break;
			case 'http://rage.deploy2.ftk.de/taxonomy/SHOP#59bfe60d4f26c':
				for (var i=0; i<classifications[name].length; i++)
				{
					var concept = classifications[name][i];
					//console.log('concept',concept);
					document.getElementById('14shop-input').value = concept;
				}
				if (classifications[name].length>1)
				{
					console.error('Only one concept from ',name,' is read, the rest are ignored');
				}
				break;
			case 'http://rageproject.eu/taxonomy/asset-type#asset-type':
				//addCompatList('Inappropriate taxonomy');
				console.error('Inappropriate taxonomy '+name);
				console.dir(elem);
				break;
			default: {
				//addCompatList('Unknown taxonomy');
				console.error('Unknown or unsupported taxonomy '+name);
				console.dir(elem);
			}
		}
	}
}



function readClassificationContext( context )
{
	// ignore attributes
	
	// scan elements and collect used taxonomies
	var taxonomy;
	for (var elem=findFirstElementChild(context); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'title': /*addCompatList('Ignored classification title');*/ break; // ignore title
			case 'description': break; // ignore description
			case 'themeTaxonomy':
					taxonomy = elem.getAttribute('rdf:resource');
					classifications[taxonomy] = classifications[taxonomy]||[];
					break;
			case 'theme':
					if (!taxonomy)
					{
						//addCompatList('Missing taxonomy tags');
						console.error('Concept tag without taxonomy tag before it');
					}
					var concept = elem.getAttribute('rdf:resource');
					if (classifications[taxonomy].indexOf(concept)<0)
						classifications[taxonomy].push(concept);
					break;
			default: {
				//addCompatList('Unknown classification context tag');
				console.error('Unknown and unsupported classification context tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
}


function readArtefactSection( usage, sectionId )
{
	// ignore attributes
	//var attribs = {};
	//for (var i=0; i<usage.attributes.length; i++)
	//{
	//	var attr = usage.attributes[i];
	//	attribs[attr.name] = attr.nodeValue;
	//}
	
	// scan usage elements
	for (var elem=findFirstElementChild(usage); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'description': /*addCompatList('Ignored artefact descriptions');*/ break; // ignored
			case 'CustomMetadata': /*addCompatList('Ignored artefact custom metadata');*/ break; // ignore
			case 'Artefact': readArtefact(elem,sectionId); break;
			case 'progLanguage': readProgLanguage(elem); break;
			case 'gamePlatform': readGamePlatform(elem); break;
			case 'gameEngine': readGameEngine(elem); break;
			default: {
				//addCompatList('Unknown artefact section tag'); 
				console.error('Unknown and unsupported artefact section tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
}



// mapping between old model and mew model -> see mew.producers.js
function readArtefact( artefact,sectionId )
{
	// scan attributes
	var attrURL = desafe(artefact.getAttribute('url'));
	var attrUUID = artefact.getAttribute('uuid');
	var attrName = desafe(artefact.getAttribute('name'));
	//var attribs = {};
	//for (var i=0; i<artefact.attributes.length; i++)
	//{
	//	var attr = artefact.attributes[i];
	//	attribs[attr.name] = attr.nodeValue;
	//}
	
	// collect artefact metadata
	var title,accessURL,type,reference,format,date;
	for (var elem=findFirstElementChild(artefact); elem; elem=elem.nextElementSibling)
	{
		//console.log('>>',elem.localName);
		switch (elem.localName)
		{
			case 'title': title = elem.innerHTML||elem.textContent; break;
			case 'type': type = desafe(elem.getAttribute('rdf:resource')); break;
			case 'accessURL': accessURL = desafe(elem.getAttribute('rdf:resource')); break;
			case 'reference': reference = desafe(elem.innerHTML||elem.textContent); break;
			case 'creator': /*addCompatList('Ignored artefact creators');*/ break; // ignore
			case 'publisher': /*addCompatList('Ignored artefact publishers');*/ break; // ignore
			case 'versionInfo': /*addCompatList('Ignored artefact version infos');*/ break; // ignore
			case 'description': /*addCompatList('Ignored artefact descriptions');*/ break; // ignore
			case 'License': /*addCompatList('Ignored artefact licenses'); break;*/ // ignore
			case 'format': format = elem.innerHTML||elem.textContent||elem.getAttribute('rdf:resource').split('#')[1]||''; break;
			case 'date': date = elem.innerHTML||elem.textContent; break;
			default: {
				//addCompatList('Unsupported artefact tag'); 
				console.error('Unknown and unsupported artefact tag '+elem.localName);
				console.dir(elem);
			}
		}
	}

	/*
	console.log('ARTEFACT\n\ttitle =',title,
		'\n\taccessURL=',accessURL,
		'\n\ttype=',type,
		'\n\treference=',reference,
		'\n\tmeta section=',sectionId,
		'\n\tmew section=',mewId);
	*/
	
	// if it is asset logo, process it now
	if (reference)
	if (reference.indexOf('_rage_logo_/')==0)
	{
		// it is a local hosted-in-the-asset file
		var fileElem = document.getElementById('43-input');
		fileElem.isReal = true;
		fileElem.isNew = false;
		fileElem.isFile = true;
		fileElem.isDeleted = false;
		fileElem.mewAttrURL = attrURL;
		fileElem.mewAttrUUID = attrUUID;
		fileElem.mewAccessURL = accessURL;
		fileElem.mewName = title;
		fileElem.mewReference = reference;
		fileElem.src = accessURL;
		return;
	}
	
	// decide MEW section based on type and metadata section
	var mewId;
	if (reference=='_rage_youtube_')
		mewId = 'artefact-media';
	else if (reference && reference.indexOf('_rage_snapshot_/')==0)
		mewId = 'artefact-media';
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#source-code'
		|| (!type && sectionId=='Implementation')) mewId = 'artefact-sources';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#documentation'
		|| type=='http://rageproject.eu/taxonomy/artefct-type#tutorial'
		|| (!type && sectionId=='Design')) mewId = 'artefact-docs';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#demonstration'
		|| type=='http://rageproject.eu/taxonomy/artefct-type#setup-config'
		|| (!type && sectionId=='Usage')) mewId = 'artefact-configs';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#test'
		|| (!type && sectionId=='Tests')) mewId = 'artefact-tests';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#data'
		|| (!type && sectionId=='Requirements')) mewId = 'artefact-others';
	else
	{
		//addCompatList('Cannot map artefact type'); 
		console.error ('Cannot map artefact from the full metadata model to the MEW metadata model');
		console.log('ARTEFACT\n\ttitle =',title,'\n\taccessURL=',accessURL,'\n\ttype=',type,'\n\treference=',reference);
	}
	
//console.log('=>',reference);	
	if (reference && reference!='_rage_youtube_')
	{
		// it is a local hosted-in-the-asset file
		var fileElem = addFileNoSelect(mewId,mewId=='artefact-media'?'artefact-snapshot-template':'artefact-no-select-template');
		fileElem.isReal = true;
		fileElem.isNew = false;
		fileElem.isFile = true;
		fileElem.isDeleted = false;
		fileElem.path = mewId=='artefact-media'?'_rage_snapshot_':'';
		fileElem.mewAttrURL = attrURL;
		fileElem.mewAttrUUID = attrUUID;
		fileElem.mewAccessURL = accessURL;
		fileElem.getElementsByClassName('artefact-filename')[0].innerHTML = reference.replace('_rage_snapshot_/','');
		fileElem.getElementsByClassName('main-icon')[0].src = (mewId=='artefact-media'?'images/snapshot.png':'images/file.png');
		if (date)
			fileElem.getElementsByClassName('artefact-date')[0].innerHTML = date;
		else
			fileElem.getElementsByClassName('artefact-date-section')[0].style.display = "none";
		
		if (format)
			fileElem.getElementsByClassName('artefact-format')[0].innerHTML = format;
		else
			fileElem.getElementsByClassName('artefact-format-section')[0].style.display = "none";
	}
	else
	{
		// it is a link to external file
		var linkElem = addLink(mewId,mewId=='artefact-media'?'artefact-youtube-template':'artefact-link-template');
		linkElem.isReal = true;
		linkElem.isNew = false;
		linkElem.isFile = false;
		linkElem.isDeleted = false;
		linkElem.path = (mewId=='artefact-media')?'_rage_youtube_':'';
		linkElem.mewAttrURL = attrURL;
		linkElem.mewAttrUUID = attrUUID;
		linkElem.getElementsByClassName('artefact-filename')[0].value = accessURL||title||attrName;
	}
}


function readAssetSolution( solution )
{
	// ignore attributes
	//for (var i=0; i<asset.attributes.length; i++)
	//{
	//	var attr = asset.attributes[i];
	//	persist.assetAttr[attr.name] = attr.nodeValue;
	//}
	
	// scan elements
	for (var elem=findFirstElementChild(solution); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'description': /*addCompatList('Ignored solution descriptions');*/ break; // ignored
			case 'Design': readArtefactSection(elem,'Design'); break;
			case 'Implementation': readArtefactSection(elem,'Implementation'); break;
			case 'Tests': readArtefactSection(elem,'Tests'); break;
			case 'Requirements': readArtefactSection(elem,'Requirements'); break;
			default: {
				//addCompatList('Unknown solution tag'); 
				console.error('Unknown and unsupported solution tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
}


function readProgLanguage( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('23-input');
	
	target.value = value;
}


function readGameEngine( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('21-input');
	
	target.value = value;
}


function readGamePlatform( dom )
{
	var value = dom.getAttribute('rdf:resource');
	var target = document.getElementById('22-input');
	
	target.value = value;
}


/* NO RELATED ASSETS
function readRelatedAsset( relation )
{
	var name,minVersion,maxVersion,relationType;
	
	// read attributes
	for (var i=0; i<relation.attributes.length; i++)
	{
		var attr = relation.attributes[i];
		switch (attr.name)
		{
			case 'name': name = attr.nodeValue; break;
			case 'minVersion': minVersion = attr.nodeValue; break;
			case 'maxVersion': maxVersion = attr.nodeValue; break;
			// ignore all others
		}
	}
	
	// scan elements
	for (var elem=findFirstElementChild(relation); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'description': break; // silently ignored
			case 'accessURL': addCompatList('Ignored related software asset accessURL'); break; // ignored
			case 'relationType': relationType = elem.innerHTML; break;
			default: {
				addCompatList('Ignored related software asset tag'); 
				console.error('Unknown and unsupported related software asset tag '+elem.localName);
				console.dir(elem);
			}
		}
	}
	
	switch (relationType)
	{
		case 'prevVersion':
			var host = document.getElementById('24-input');
			host.value = name;
			if (host.value!=name)
			{
				// force insert it
				host.innerHTML += '<option value="'+name+'">'+name+'</option>';
				host.value = name;
			}
			document.getElementById('26-input').value = maxVersion||minVersion;
			break;
		case 'nextVersion':
			var host = document.getElementById('25-input');
			host.value = name;
			if (host.value!=name)
			{
				// force insert it
				host.innerHTML += '<option value="'+name+'">'+name+'</option>';
				host.value = name;
			}
			document.getElementById('27-input').value = minVersion||maxVersion;
			break;
		default: {
			addCompatList('Ignored related software asset type'); 
			console.error('Unsupported related software asset type '+relationType);
		}
	}
}
NO RELATED ASSETS */