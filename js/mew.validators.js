﻿var warningElem;
var warningMetadataName;

var custom_metadata_completeness;

var MAX_TEXT_LENGTH = 4000;

var progress = [
	// TOTAL MAX = 200
	
	// STEP 1 MAX = 32
	{step:1, item:'1-warning', max:10, penalty: 0, mandatory:true, info:'title'},
	{step:1, item:'2-warning', max:10, penalty: 0, mandatory:true, info:'description'},
	{step:1, item:'41-warning', max:10, penalty: 0, mandatory:true, info:'promotional description'},
	{step:1, item:'44-warning', max:10, penalty: 0, mandatory:true, info:'technical description'},
	{step:1, item:'43-warning', max:10, penalty: 0, mandatory:true, info:'logo'},
	{step:1, item:'4-warning', max:5,  penalty: 0, mandatory:false, info:'date'},
	{step:1, item:'5-warning', max:2,  penalty: 0, mandatory:false, info:'language'},
	{step:1, item:'6-warning', max:5,  penalty: 0, mandatory:false, info:'access url'},
	
	// STEP 2 MAX = 28
	/*{step:2, item:'3-warning', max:10, penalty: 0, mandatory:true, info:'type'}, REMOVAL REQUESTED BY WIM, 2017.04.18*/
	{step:2, item:'21-warning', max:3,  penalty: 0, mandatory:false, info:'game engine'},
	{step:2, item:'22-warning', max:3,  penalty: 0, mandatory:false, info:'game platform'},
	{step:2, item:'23-warning', max:6,  penalty: 0, mandatory:false, info:'programming language'},
	/*{step:2, item:'7-warning', max:2,  penalty: 0, mandatory:false, info:'game genre'}, REMOVAL REQUESTED BY WIM, 2017.04.18*/
	{step:2, item:'14-warning', max:3,  penalty: 0, mandatory:false, info:'learning goals'},
	{step:2, item:'14shop-warning', max:3,  penalty: 0, mandatory:false, info:'shop'},
	/*{step:2, item:'15-warning', max:2,  penalty: 0, mandatory:false, info:'knowledge skills'}, REMOVAL REQUESTED BY WIM, 2017.04.18*/
	{step:2, item:'16-warning', max:10,  penalty: 0, mandatory:true, info:'applied computing'},
	/*{step:2, item:'34-warning', max:8,  penalty: 0, mandatory:true, info:'game genre/learning goals/knowledge skills/applied computing'}, CHANGE REQUESTED BY WIM, 2017.04.18*/
	{step:2, item:'8-warning', max:3,  penalty: 0, mandatory:false, info:'keywords'},
	
	// STEP 3 MAX = 26
	{step:3, item:'9-warning', max:10,  penalty: 0, mandatory:true, info:'version'},
	{step:3, item:'10-warning', max:8,  penalty: 0, mandatory:true, info:'version notes'},
	{step:3, item:'11-warning', max:8,  penalty: 0, mandatory:true, info:'development status'},
	/*{step:3, item:'12-warning', max:5,  penalty: 0, mandatory:false, info:'maturity level'}, REMOVAL REQUESTED BY WIM, 2017.04.18 */
/* NO RELATED ASSETS	
	//{step:3, item:'24-warning', max:2,  penalty: 0, mandatory:false, info:'previous version name'},
	//{step:3, item:'25-warning', max:2,  penalty: 0, mandatory:false, info:'next version name'},
	{step:3, item:'26-warning', max:2,  penalty: 0, mandatory:false, info:'previous version number'},
	{step:3, item:'27-warning', max:2,  penalty: 0, mandatory:false, info:'next version number'},
NO RELATED ASSETS */	
	//{step:3, item:'13-warning', max:0,  penalty: 0, mandatory:false, info:'custom metadata'}, REMOVAL REQUESTED BY WIM, 2017.04.18
	{step:3, item:'40-warning', max:5,  penalty: 0, mandatory:false, info:'commit URI'},
	
	// STEP 4 MAX = 25
	{step:4, item:'17-warning', max:10, penalty: 0, mandatory:true, info:'license type'},
	{step:4, item:'18-warning', max:5, penalty: 0, mandatory:false, info:'license url'},
	{step:4, item:'19-warning', max:10, penalty: 0, mandatory:true, info:'license conditions'},
	
	// STEP 5 MAX = 25
	{step:5, item:'creators-20-warning-name', max:10, penalty: 0, mandatory:true, info:'creator name'},
	{step:5, item:'creators-20-warning-homepage', max:5, penalty: 0, mandatory:false, info:'creator homepage'},
	{step:5, item:'creators-20-warning-mailbox', max:3, penalty: 0, mandatory:false, info:'creator mail box'},
	/* REMOVAL REQUESTED BY WIM, 2017.04.18
	{step:5, item:'publishers-20-warning-name', max:10, penalty: 0, mandatory:true, info:'publisher name'},
	{step:5, item:'publishers-20-warning-homepage', max:5, penalty: 0, mandatory:false, info:'publisher homepage'},
	{step:5, item:'publishers-20-warning-mailbox', max:3, penalty: 0, mandatory:false, info:'publisher mail box'},
	*/
	{step:5, item:'owners-20-warning-name', max:3, penalty: 0, mandatory:true, info:'owner name'},
	{step:5, item:'owners-20-warning-homepage', max:2, penalty: 0, mandatory:false, info:'owner homepage'},
	{step:5, item:'owners-20-warning-mailbox', max:2, penalty: 0, mandatory:false, info:'owner mail box'},
	
	// STEP 6 MAX = 35
	{step:6, item:'42-warning', max:5, penalty: 0, mandatory:false, info:'detailed description'},
	{step:6, item:'28-warning', max:10, penalty: 0, mandatory:true, info:'artefacts - sources'},
	{step:6, item:'29-warning', max:10, penalty: 0, mandatory:true, info:'artefacts - user documentation'},
	{step:6, item:'30-warning', max:10, penalty: 0, mandatory:false, info:'artefacts - install & config'},
	{step:6, item:'31media-warning', max:5, penalty: 0, mandatory:false, info:'artefacts - youtube, snapshots'},
	{step:6, item:'31-warning', max:5, penalty: 0, mandatory:false, info:'artefacts - testing'},
	{step:6, item:'32-warning', max:0, penalty: 0, mandatory:false, info:'artefacts - others'},
	/*{step:6, item:'33-warning', max:0, penalty: 0, mandatory:false, info:'artefacts - local folder'}, REMOVAL REQUESTED BY WIM, 2017.04.18*/
	
	// STEP 7 MAX = 25
	{step:7, item:'35-warning', max:5, penalty: 0, mandatory:false, info:'software quality'},
	{step:7, item:'36-warning', max:5, penalty: 0, mandatory:false, info:'architecture'},
	//{step:7, item:'37-warning', max:5, penalty: 0, mandatory:true, info:'interoperability'}, REMOVAL REQUESTED BY WIM, 2017.04.18
	{step:7, item:'38-warning', max:5, penalty: 0, mandatory:false, info:'software testing'},
	{step:8, item:'39-warning', max:10, penalty: 0, mandatory:true, info:'self-declaration'}, //RELOCATION REQUESTED BY DOMINIQUE, 2018.05.09
]

var licenseURL = {
	"http://rageproject.eu/taxonomy/licns-type#apache-2-0": "https://opensource.org/licenses/Apache-2.0",
	"http://rageproject.eu/taxonomy/licns-type#bsd-2-clause": "https://opensource.org/licenses/BSD-2-Clause",
	"http://rageproject.eu/taxonomy/licns-type#bsd-3-clause": "https://opensource.org/licenses/BSD-3-Clause",
	"http://rageproject.eu/taxonomy/licns-type#cddl-1-0": "https://spdx.org/licenses/CDDL-1.0.html",
	"http://rageproject.eu/taxonomy/licns-type#epl-1-0": "https://spdx.org/licenses/EPL-1.0.html",
	"http://rageproject.eu/taxonomy/licns-type#gpl2-gnu-genrl": "https://spdx.org/licenses/GPL-2.0.html",
	"http://rageproject.eu/taxonomy/licns-type#lgpl-gnu-librr": "https://opensource.org/licenses/lgpl-license",
	"http://rageproject.eu/taxonomy/licns-type#mit-mit-licns": "https://opensource.org/licenses/MIT",
	"http://rageproject.eu/taxonomy/licns-type#mpl-2-0": "https://spdx.org/licenses/MPL-2.0.html"
};

function isMandatory(item)
{
	for (var i=0; i<progress.length; i++)
		if (progress[i].item==item)
			return progress[i].mandatory;
	return false;
}

function setMandatory(item,value)
{
	var set = false;
	for (var i=0; i<progress.length; i++)
		if (progress[i].item==item)
		{
			progress[i].mandatory = value;
			set = true;
		}
	if (!set)
		console.log('could not set mandatory to',item);
	updateProgressBar();
}

function setMax(item,value)
{
	var set = false;
	for (var i=0; i<progress.length; i++)
		if (progress[i].item==item)
		{
			progress[i].max = value;
			progress[i].penalty = Math.min(value,progress[i].penalty);
			set = true;
		}
	if (!set)
		console.log('could not set max to',item);
	updateProgressBar();
}

function getMax(item)
{
	for (var i=0; i<progress.length; i++)
		if (progress[i].item==item)
			return progress[i].max;
}

function setPenalty(item,percent,offset)
{
	var set = false;
	offset = offset||0;
//	console.log('request penalty['+item+'] =',(100*percent).toFixed(0)+'%');
	for (var i=0; i<progress.length; i++)
		if (progress[i].item==item)
		{
			progress[i].penalty = percent*progress[i].max+offset;
//			console.log('penalty['+progress[i].info+'] =',progress[i].penalty,' ('+Math.round(100*percent)+'% of '+progress[i].max+')');
			set = true;
		}
	if (!set)
		console.log('could not set penalty to',item);
	updateProgressBar();
}

function updateProgressBar()
{
	var penalty = [0,0,0,0,0,0,0,0]; // 1 total + 6 steps
	var max = [0,0,0,0,0,0,0,0];
	var completeness = [0,0,0,0,0,0,0,0];
	var incompleteMandatory = [0,0,0,0,0,0,0,0];
	
	for (var i=0; i<progress.length; i++)
	{
		max[0] += progress[i].max;
		penalty[0] += progress[i].penalty;
		if (progress[i].mandatory && progress[i].penalty==progress[i].max)
			incompleteMandatory[0]++;
		
		var step = progress[i].step;
		max[step] += progress[i].max;
		penalty[step] += progress[i].penalty;
		if (progress[i].mandatory && progress[i].penalty==progress[i].max)
			incompleteMandatory[step]++;
	}
//console.log('max=',max);
//console.table(progress);

	for (var i=0; i<max.length; i++)
	{
		if (max[i])
			completeness[i] = Math.max(0,Math.round( 100*(1-penalty[i]/max[i]) ))+'%';
		else
			completeness[i] = 'n/a';
	}
	
	custom_metadata_completeness = completeness[0];
	
	for (var i=0; i<max.length; i++) if (document.getElementById('nav-index-final-'+i))
	{
		document.getElementById('nav-index-final-'+i).innerHTML = completeness[i];
		if (incompleteMandatory[i])
			document.getElementById('nav-text-final-'+i).innerHTML = 'Some mandatory elements are not present';
		else
			document.getElementById('nav-text-final-'+i).innerHTML = '';
		document.getElementById('nav-section-final-'+i).innerHTML = '<div class="percentage">'+completeness[i]+'</div><div class="text">'+(i?'completed':'overall completed')+'</div>';
	}
	document.getElementById('nav-section-final-00').innerHTML = '<div class="percentage">'+completeness[0]+'</div><div class="text">overall completed</div>';

	document.getElementById("final-incomplete").style.display = incompleteMandatory[0]?'block':'none';
	document.getElementById("final-complete").style.display = (incompleteMandatory[0]==0 && penalty[0]==0)?'block':'none';
	document.getElementById("final-partial").style.display = (incompleteMandatory[0]==0 && penalty[0]>0)?'block':'none';
	
	// handle the final submit button
	var enableSubmit = (options.permissions=='rw') && (incompleteMandatory[0]==0);
	enableSubmit = enableSubmit||('acem-rypy' in options); // always enable if in acem-rypy mode
	var submitButton = document.getElementById("final-submit-button");
	
	if (enableSubmit)
	{
		submitButton.style.background = 'MediumSeaGreen';
		submitButton.onclick = function(){showStep(9)};
	}
	else
	{
		submitButton.style.background = 'LightGray';
		submitButton.onclick = disabledSubmit;
	}
	
};
		
		
function stringDefault(str,value,plural)
{
	if (str==value)
	{
		if (plural)
			warningElem.innerHTML = 'These provisional '+warningMetadataName+' have been automatically created by the system. Please provide better '+warningMetadataName+' for your component.';
		else
			warningElem.innerHTML = 'This provisional '+warningMetadataName+' has been automatically created by the system. Please provide a better '+warningMetadataName+' for your component.';
		return true;
	}
}
		
		
function stringDefaultAddCustom(str,value)
{
	if (str==value)
	{
		warningElem.innerHTML = ''; //'Please, do not forget to add the actual '+warningMetadataName+' as a custom property in Step 3.'; REMOVAL REQUESTED BY WIM, 2017.04.18
		return true;
	}
}
		
		
function stringEqual(str,value,template)
{
	template = template||'This %%% is too general. If possible, select a more specific %%% for your component.';
	
	if (str==value)
	{
		warningElem.innerHTML = template.replace('%%%',warningMetadataName);
		return true;
	}
}


function stringEmpty(str,template)
{
	template = template||'The %%% cannot be left empty.';
	
	if (str.length==0)
	{
		warningElem.innerHTML = template.replace('%%%',warningMetadataName);
		return true;
	}
}


function stringEmptyOptional(str,template)
{
	if (str.length==0)
	{
		if (!template) template = ''; //'You may leave the %%% empty, but it would be better if not.' -- REMOVAL REQUESTED BY WIM, 2017.04.18
		warningElem.innerHTML = template.replace('%%%',warningMetadataName);
		return true;
	}
}

function stringRAGE(str)
{
	if (str.indexOf(rage.SERVER)>-1)
	{
		warningElem.innerHTML = 'This URL contains an internal link to the RAGE repository. Such addresses are not allowed. If you keep this address, it iwll be automatically removed later on.';
		return true;
	}
}

function stringLength(str,limit)
{
	if (str.length<limit)
	{
		//warningElem.innerHTML = 'This '+warningMetadataName+' appears to be too short. Are you sure it is OK?';
		return true;
	}
}

function stringMaxLength(str,limit)
{
	if (str.length>limit)
	{
		warningElem.innerHTML = 'This '+warningMetadataName+' appears to be too long ('+str.length+' characters). The length must be '+limit+' or less characters';
		return true;
	}
}

function stringWords(str,limit)
{
	str = str.replace(',',' ').replace(';',' ').split(' ');
	if (str.length<limit)
	{
		//warningElem.innerHTML = 'There might be too few words in this '+warningMetadataName+'. Are you sure this is OK?';
		return true;
	}
}

function stringSentence(str)
{
	str = str.split('.');
	if (str.length<3)
	{
		//warningElem.innerHTML = 'It appears there is only one sentence. Are you sure this '+warningMetadataName+' is OK?';
		return true;
	}
}

function logoImage(logo)
{
	if (!logo)
	{
		warningElem.innerHTML = 'The '+warningMetadataName+' cannot be left empty.';
		return true;
	}
}

function stringNoConnection(str)
{
	if (str=='\u22D0\u22EF\u22D1')
	{
		warningElem.innerHTML = 'No connection to the RAGE server, that provides values for the '+warningMetadataName+'. There might be a network issue (e.g. no internet) or permission issue (e.g. cross-origin access violation). We do not know. Sorry. m(_ _)m';
		return true;
	}
}

function stringVersion(str)
{
	str = str.split('.');
	if (str.length<2)
	{
		warningElem.innerHTML = 'There are too few elements in the '+warningMetadataName+'. The shortest format is <em>major.minor</em>.';
		return true;
	}
	if (str.length>4)
	{
		warningElem.innerHTML = 'There are too many elements in the '+warningMetadataName+'. The longest format is <em>major.minor.rev.build</em>.';
		return true;
	}
	for (var i=0; i<str.length; i++)
	{
		if (parseInt(str[i])+'' != str[i])
		{
			warningElem.innerHTML = 'There is a problem with the '+(i+1)+'<sup>'+(['st','nd','rd','th'])[i]+'</sup> element of the '+warningMetadataName+'. It does not appear to be an integer decimal number.';
			return true;
		}
	}
}

function stringPhrases(str,limit)
{
	str = str.split(',');
	if (str.length<limit)
	{
		warningElem.innerHTML = 'There might be too few phrases in this '+warningMetadataName+'. It might be better to have '+limit+'-'+(limit+2)+' comma separated words or phrases.';
		return true;
	}
}

function stringDateInvalid(str)
{
	str = str.split('-');
	if (str.length!=3)
	{
		warningElem.innerHTML = 'It appears the '+warningMetadataName+' you entered is not formatted as YYYY-MM-DD or is not a complete date. Could you fix that, please?';
		return true;
	}
	// check date
	var day = +str[2], month = +str[1], year = +str[0];
    var date = new Date(year,month-1,day);
    if(date.getFullYear()!=year || date.getMonth()!=month-1 || date.getDate()!=day)
	{
		warningElem.innerHTML = 'The '+warningMetadataName+' you entered is invalid. You may use the calendar button to pick a valid date or type a date using the format <em>YYYY-MM-DD</em>. For example, Jan 15, 2017 would be 2017-01-15.';
		return true;
	}
}

function stringDateStrange(str)
{
	str = str.split('-');
	var year = +str[0];
	if (year<2016)
	{
		warningElem.innerHTML = 'Are you sure that the '+warningMetadataName+' you picked or entered is correct? The year is too far in the past.';
		return true;
	}
	
	if (year>new Date().getFullYear())
	{
		warningElem.innerHTML = 'The year in the '+warningMetadataName+' is in the future. Are you sure this is OK?';
		return true;
	}
}


function stringMetadata(str)
{
	// format:  property=value, property=value, ...
	if (!str) return false; // empty, ok
	
	str = str.split(',');
//	console.log(str);
	for (var i=0; i<str.length; i++)
	{
		var pair = str[i].split('=');
		if (pair.length!=2)
		{
			warningElem.innerHTML = 'Unfortunately it is hard to split the text <em>"'+str[i].trim()+'"</em> into a pair like <em>name=value</em>. Could you fix this?';
			return true;
		}
		if (pair[0].trim()=='')
		{
			warningElem.innerHTML = 'Unfortunately it is hard to find the property name in this text <em>"'+str[i]+'"</em>. The name is what stays to the left of <em>"="</em>. Could you fix this?';
			return true;
		}
	}
}

function stringURL(str)
{
	str = str.trim();
	
	// http://stackoverflow.com/questions/8188645/javascript-regex-to-match-a-url-in-a-field-of-text
	var pattern = new RegExp( '(http|ftp|https)://[\\w-]+(\\.[\\w-]+)+([\\w-.,@?^=%&:/~+#-]*[\\w@?^=%&;/~+#-])?' );
 	
	if (!pattern.test(str) && str.substr(0,4)!='http' && str.substr(0,3)!='ftp')
	{
		warningElem.innerHTML = 'The '+warningMetadataName+' looks like an invalid URL. Are you sure you have <em>http://</em>, <em>https://</em> or <em>ftp://</em>?';
		return true;
	}
	if (!pattern.test(str))
	{
		warningElem.innerHTML = 'The '+warningMetadataName+' looks like an invalid URL. Are you sure it is correct?';
		return true;
	}
}

function stringPath(str,text)
{
	var pattern = /^(\/{0,1}[a-z_\.\ \-%0-9\.]+\/{0,1})*$/igm;
		
	text = text.replace('%%%',warningMetadataName);
	
	if (str.indexOf(' ')>=0)
	{
		warningElem.innerHTML = 'There is a space symbol in the '+warningMetadataName+'. This is not illegal, just unusual.';
		return false;
	}
	if (!pattern.test(str.trim()))
	{
		warningElem.innerHTML = text;
		return true;
	}
}

function stringEMail(str)
{
	// http://stackoverflow.com/questions/46155/validate-email-address-in-javascript
	var pattern = /\S+@\S+\.\S+/;
		
	if (!pattern.test(str))
	{
		warningElem.innerHTML = 'The '+warningMetadataName+' looks like an invalid e-mail address. Are you sure it is correct?';
		return true;
	}
	if (str.indexOf(':')>=0 && str.indexOf('mailto:')!=0)
	{
		warningElem.innerHTML = 'The '+warningMetadataName+' looks like an invalid e-mail address. If there is a protocol name, it must be <em>mailto:</em>.';
		return true;
	}
}

// http://homepages.math.uic.edu/~leon/mcs425-s08/handouts/char_freq2.pdf
var englishPairs = [
	[1,20,33,52,0,12,18,5,39,1,12,57,26,181,1,20,1,75,95,104,9,20,13,1,26,1],
	[11,1,0,0,47,0,0,0,6,1,0,17,0,0,19,0,0,11,2,1,21,0,0,0,11,0],
	[31,0,4,0,38,0,0,38,10,0,18,9,0,0,45,0,1,11,1,15,7,0,0,0,1,0],
	[48,20,9,13,57,11,7,25,50,3,1,11,14,16,41,6,0,14,35,56,10,2,19,0,10,0],
	[110,23,45,126,48,30,15,33,41,3,5,55,47,111,33,28,2,169,115,83,6,24,50,9,26,0],
	[25,2,3,2,20,11,1,8,23,1,0,8,5,1,40,2,0,16,5,37,8,0,3,0,2,0],
	[24,3,2,2,28,3,4,35,18,1,0,7,3,4,23,1,0,12,9,16,7,0,5,0,1,0],
	[114,2,2,1,302,2,1,6,97,0,0,2,3,1,49,1,0,8,5,32,8,0,4,0,4,0],
	[10,5,32,33,23,17,25,6,1,1,8,37,37,179,24,6,0,27,86,93,1,14,7,2,0,2],
	[2,0,0,0,2,0,0,0,3,0,0,0,0,0,3,0,0,0,0,0,8,0,0,0,0,0],
	[6,1,1,1,29,1,0,2,14,0,0,2,1,9,4,0,0,0,5,4,1,0,2,0,2,0],
	[40,3,2,36,64,10,1,4,47,0,3,56,4,2,41,3,0,2,11,15,8,3,5,0,31,0],
	[44,7,1,1,68,2,1,3,25,0,0,1,5,2,29,11,0,3,10,9,8,0,4,0,18,0],
	[40,7,25,146,66,8,92,16,33,2,8,9,7,8,60,4,1,3,33,106,6,2,12,0,11,0],
	[16,12,13,18,5,80,7,11,12,1,13,26,48,106,36,15,0,84,28,57,115,12,46,0,5,1],
	[23,1,0,0,30,1,0,3,12,0,0,15,1,0,21,10,0,18,5,11,6,0,1,0,1,0],
	[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,9,0,0,0,0,0],
	[50,7,10,20,133,8,10,12,50,1,8,10,14,16,55,6,0,14,37,42,12,4,11,0,21,0],
	[67,11,17,7,74,11,4,50,49,2,6,13,12,10,57,20,2,4,43,109,20,2,24,0,4,0],
	[59,10,11,7,75,9,3,330,76,1,2,17,11,7,115,4,0,28,34,56,17,1,31,0,16,0],
	[7,5,12,7,7,2,14,2,8,0,1,34,8,36,1,16,0,44,35,48,0,0,2,0,1,0],
	[5,0,0,0,65,0,0,0,11,0,0,0,0,0,4,0,0,0,0,0,0,0,0,0,1,0],
	[66,1,1,2,39,1,0,44,39,0,0,2,1,12,29,0,0,3,4,4,1,0,2,0,1,0],
	[1,0,2,0,1,0,0,0,2,0,0,0,0,0,0,3,0,0,0,3,0,0,0,0,0,0],
	[18,7,6,6,14,7,3,10,11,1,1,4,6,3,36,4,0,3,19,20,1,1,12,0,2,0],
	[1,0,0,0,3,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
];
	
function stringEnglish(str,limit)
{
	limit = limit||20;
	var sum = 0, pairs = [], charSet = [];
	str = str.toUpperCase();
	for (var i=0; i<str.length-1; i++)
	{
		var code1 = str.charCodeAt(i)-'A'.charCodeAt();
		var code2 = str.charCodeAt(i+1)-'A'.charCodeAt();
		if (code1>=0 && code1<26)
		if (code2>=0 && code2<26)
		{
			sum += englishPairs[code1][code2]-15;
			var chars = str.substr(i,2);
			if (pairs.indexOf(chars)<0) pairs.push(chars);
		}
	}
	for (var i=0; i<26; i++) charSet[i]=0;
	for (var i=0; i<str.length; i++)
	{
		var code = str.charCodeAt(i)-'A'.charCodeAt();
		if (code>=0 && code<26) charSet[code]++;
	}
	var diffChars = 0;
	for (var i=0; i<26; i++) if (charSet[i]) diffChars++;
	
	var variability = 10*pairs.length/str.length; // number of different character pairs
	var englishability = sum/str.length; // total statistical weight of character pairs
	var charactability = Math.min(str.length,26)/diffChars; // diversity of characters
	
	//console.log('English:',variability.toFixed(2),englishability.toFixed(2),-charactability.toFixed(2));
	//console.log('English:',variability.toFixed(2),englishability.toFixed(2),-charactability.toFixed(2),'limit='+limit);
	if (variability+englishability-charactability<limit)
	{
		warningElem.innerHTML = 'Are you sure the text in '+warningMetadataName+' is in English?';
		return true;
	}
}

function validateAssetTitle(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'name';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	if (stringDefault(str,'Empty RAGE Component')) return setPenalty(id,0.5);
	if (stringLength(str,10)) return setPenalty(id,0.5);
	if (stringWords(str,3)) return setPenalty(id,0.3);
	if (stringEnglish(str)) return setPenalty(id,0.2);
	updateProgressBar();
}


/* REMOVAL REQUESTED BY WIM, 2017.04.18
function validateLocalFolder(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'local folder name';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (str && stringPath(str,'This local path does not look well formatted. It should look like <em>some/local/path</em>. Now, if you add files to the component, they will be associated with this strange path. Are you sure?')) return setPenalty(id,0);
	updateProgressBar();
}
*/

function validateAssetDescription(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'one sentence description';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	if (stringDefault(str,'Empty RAGE Comopnent description.')) return setPenalty(id,0.5);
	if (stringLength(str,30)) return setPenalty(id,0.2);
	if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
	if (stringWords(str,10)) return setPenalty(id,0.1);
	//if (stringSentence(str)) return setPenalty(id,0.1); -- the hint says "one sentence description
	if (stringEnglish(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetPromotionalDescription(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'short non-technical description';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	if (stringLength(str,30)) return setPenalty(id,0.2);
	if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
	if (stringWords(str,10)) return setPenalty(id,0.1);
	if (stringSentence(str)) return setPenalty(id,0.1);
	if (stringEnglish(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetTechnicalDescription(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'technical description';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	if (stringLength(str,30)) return setPenalty(id,0.2);
	if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
	if (stringWords(str,10)) return setPenalty(id,0.1);
	if (stringSentence(str)) return setPenalty(id,0.1);
	if (stringEnglish(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetDetailedDescription(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'detailed description';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1.0);
	if (stringLength(str,30)) return setPenalty(id,0.2);
	if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
	if (stringWords(str,10)) return setPenalty(id,0.1);
	if (stringSentence(str)) return setPenalty(id,0.1);
	if (stringEnglish(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetLogo(elem)
{
	var logo = Boolean(elem.mewLogo || elem.mewAccessURL || false);
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'component logo';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (logoImage(logo)) return setPenalty(id,1.0);
	updateProgressBar();
}

function validateLicenseConditions(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'conditions and restrictions';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (licenseURL[document.getElementById('17-input').value])
		document.getElementById('licenseDescr').style.display = 'none';
	else
	{
		document.getElementById('licenseDescr').style.display = 'block';
		if (stringEmpty(str)) return setPenalty(id,1.0);
		if (stringLength(str,10)) return setPenalty(id,0.2);
		if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
		if (stringEnglish(str)) return setPenalty(id,0.5);
	}
	updateProgressBar();
}

function validateAssetDate(elem,id)
{
	var str = elem.value;

	warningMetadataName = 'date';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1.0);
	if (stringDateInvalid(str)) return setPenalty(id,1.0);
	if (stringDateStrange(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetLanguage(elem)
{
	var str = elem.value.trim();
	if (str=='')
	{	// REQUESTED BY WIM, 2017.04.18
		str = 'English';
		elem.value = 'English';
	}
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'language';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1.0);
	if (stringLength(str,2)) return setPenalty(id,0.5);
	if (stringEnglish(str)) return setPenalty(id,0.3);
	updateProgressBar();
}

function validateAssetVersion(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'version';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1);
	if (stringDefault(str,'0.1')) return setPenalty(id,0.0);
	if (stringVersion(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

/* NO RELATED ASSETS
function validateRelatedAssetVersion(elem,refElemId)
{
	var str = elem.value;
	var assetName = document.getElementById(refElemId).value;

	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'version';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (str=='')
	{	// empty is OK if assetName allows it
		if (assetName)
		{
			if (stringEmpty(str)) return setPenalty(id,1);
		}
	}
	if (stringVersion(str)) return setPenalty(id,0.5);
	updateProgressBar();
}
NO RELATED ASSETS */

function validateAssetAccessURL(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'access URL';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1);
	if (stringURL(str)) return setPenalty(id,0.5);
	if (stringRAGE(str)) return setPenalty(id,1);
	updateProgressBar();
}

function validateLicenseURL(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'license URL';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1.0);
	if (stringURL(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetKeywords(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'list of keywords';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1.0);
	if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
	if (stringPhrases(str,3)) return setPenalty(id,0.1);
	if (stringEnglish(str,14)) return setPenalty(id,0.5);
	updateProgressBar();
}

function validateAssetVersionNotes(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'version notes';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	if (stringDefault(str,'Initial version',true)) return setPenalty(id,0.1);
	if (stringLength(str,10)) return setPenalty(id,0.2);
	if (stringMaxLength(str,MAX_TEXT_LENGTH)) return setPenalty(id,1);
	if (stringWords(str,5)) return setPenalty(id,0.1);
	if (stringEnglish(str)) return setPenalty(id,0.5);
	updateProgressBar();
}


/* REMOVAL REQUESTED BY WIM, 2017.04.18
function validateAssetMaturityLevel(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'maturity level';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1.0);
	if (stringLength(str,4)) return setPenalty(id,0.2);
	if (stringEnglish(str)) return setPenalty(id,0.5);
	updateProgressBar();
}
*/


function validateAssetDevelopmentStatus(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'development status';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	if (str=='http://purl.org/adms/status/Withdrawn' || str=='http://purl.org/adms/status/Deprecated')
	{
		warningElem.innerHTML = 'We are sorry to hear this.';
		return setPenalty(id,0.0);
	}
	updateProgressBar();
}

function validateCommitURI(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'commit URI';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,1);
	if (stringURL(str)) return setPenalty(id,0.5);
	updateProgressBar();
}

/* REMOVAL REQUESTED BY WIM, 2017.04.18
function validateAssetCustomMetadata(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = '';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,0);
	if (stringMetadata(str)) return setPenalty(id,1,1);
	updateProgressBar();
}
*/

/* REMOVAL REQUESTED BY WIM, 2017.04.18
function validateAssetType(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'type';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringNoConnection(str)) return setPenalty(id,1);
	if (stringEmpty(str)) return setPenalty(id,1);
	if (stringEqual(str,'http://rageproject.eu/taxonomy/asset-type#rage-asset')) return setPenalty(id,0.1);
	updateProgressBar();
}*/

/* NO RELATED ASSETS
function validateRelatedAssetName(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';
	var subId = parseInt(elem.getAttribute('id'))+'-block';
	var mandId = (parseInt(elem.getAttribute('id'))+2)+'-warning';

	warningMetadataName = 'name';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';

	//setPenalty(id,0);
	if (stringNoConnection(str));// return setPenalty(id,0);
	
	validateRelatedAssetVersion(document.getElementById("26-input"),'24-input');
	validateRelatedAssetVersion(document.getElementById("27-input"),'25-input');

	if (str=='' || str=='No such version') // empty or no connection
	{
		setMax(mandId,0);
		setMandatory(mandId,false);
		document.getElementById(subId).style.display = 'none';
	}
	else
	{
		setMax(mandId,2);
		setMandatory(mandId,true);
		document.getElementById(subId).style.display = 'block';
	}
	
	updateProgressBar();
//	console.table(progress);
}
NO RELATED ASSETS */

function validateGameEngine(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'game engine';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';

	setPenalty(id,0);
	if (stringNoConnection(str)) return setPenalty(id,1);
	if (stringEmptyOptional(str)) return setPenalty(id,1);
	//if (stringDefaultAddCustom(str,'http://rageproject.eu/taxonomy/game-engine#other')) return setPenalty(id,0); REMOVAL REQUESTED BY WIM, 2017.04.18
	updateProgressBar();
}

function validateGamePlatform(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'target platform';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringNoConnection(str)) return setPenalty(id,1);
	if (stringEmptyOptional(str)) return setPenalty(id,1);
	//if (stringDefaultAddCustom(str,'http://rageproject.eu/taxonomy/game-platfrm#other')) return setPenalty(id,0); REMOVAL REQUESTED BY WIM, 2017.04.18
	updateProgressBar();
}

function validateShop(elem)
{
	var str = elem.value;
	var id = elem.getAttribute('id').split('-')[0]+'-warning';

	warningMetadataName = 'shop';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringNoConnection(str)) return setPenalty(id,1);
	if (stringEmptyOptional(str)) return setPenalty(id,1);
	updateProgressBar();
}

function validateProgrammingLanguage(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'programming language';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringNoConnection(str)) return setPenalty(id,1);
	if (stringEmptyOptional(str)) return setPenalty(id,1);
	//if (stringDefaultAddCustom(str,'http://rageproject.eu/taxonomy/progrmmng-langg#other')) return setPenalty(id,0); REMOVAL REQUESTED BY WIM, 2017.04.18
	updateProgressBar();
}

function knownLicenseType(url)
{
    for (var type in licenseURL)
        if (licenseURL.hasOwnProperty(type) && licenseURL[type]==url)
            return true;
    return false;
}

function changeLicenseType(elem)
{
	validateLicenseType(elem);
	var type = elem.value;
	var elemURL = document.getElementById('18-input')
	if (licenseURL[type])
	{
		elemURL.value = licenseURL[type];
		validateLicenseURL(elemURL);
		document.getElementById('licenseDescr').style.display = 'none';
	}
	else
	{
		if (knownLicenseType(elemURL.value.trim()))
		{
			elemURL.value = '';
			validateLicenseURL(elemURL);
		}
		document.getElementById('licenseDescr').style.display = 'block';
	}
	validateLicenseConditions(document.getElementById('19-input'));
	updateProgressBar();
}

function validateLicenseType(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'license type';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringNoConnection(str)) return setPenalty(id,1);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	
	updateProgressBar();
}

function validateAllContacts(warningId,elem)
{
	if (typeof elem == 'string' || elem instanceof String)
		elem = document.getElementById(elem);
	else
	{
		while (elem.tagName.toUpperCase()!='TABLE') elem = elem.parentNode; // go up to <table> tag
		elem = elem.parentNode; // select <div> containing all tables
	}
	var inputs = elem.getElementsByTagName('input');
	var warnings = elem.getElementsByTagName('p');

	var id = elem.getAttribute('id')+'-'+warningId;
	
	var count = (inputs.length/3); // scale of penalties

	function validateContactName(elem,wElem)
	{
		var str = elem.value;
		warningElem = wElem;
		warningMetadataName = 'name';
		warningElem.innerHTML = '';
		if (stringEmpty(str)) return 1;
		if (stringLength(str,2)) return 0.2;
		return 0;
	}
	
	function validateContactHomepage(elem,wElem)
	{
		var str = elem.value;
		warningElem = wElem;
		warningMetadataName = 'home page';
		warningElem.innerHTML = '';
		if (stringEmptyOptional(str)) return 0.2;
		if (stringURL(str)) return 0.5;
		return 0;
	}
	
	function validateContactMailbox(elem,wElem)
	{
		var str = elem.value;
		warningElem = wElem;
		warningMetadataName = 'mailbox';
		warningElem.innerHTML = '';
		if (stringEmptyOptional(str)) return 0.2;
		if (stringEMail(str)) return 0.5;
		return 0;
	}
	
	if (count)
	{
		setPenalty(id+'-name',0);
		setPenalty(id+'-homepage',0);
		setPenalty(id+'-mailbox',0);
		var penaltyName = 0;
		var penaltyHomepage = 0;
		var penaltyMailbox = 0;
		for (var i=0; i<count; i++)
		{
			var p = validateContactName(inputs[3*i],warnings[3*i]);
			penaltyName = Math.max(penaltyName,p);
			var q = validateContactHomepage(inputs[3*i+1],warnings[3*i+1]);
			penaltyHomepage = Math.max(penaltyHomepage,p==1?1:q);
			var q = validateContactMailbox(inputs[3*i+2],warnings[3*i+2]);
			penaltyMailbox = Math.max(penaltyMailbox,p==1?1:q);
		}
		setPenalty(id+'-name',penaltyName);
		setPenalty(id+'-homepage',penaltyHomepage);
		setPenalty(id+'-mailbox',penaltyMailbox);
		
		warningElem = elem.nextElementSibling;
		warningElem.innerHTML = '';
	}
	else
	{	// no contacts
		warningElem = elem.nextElementSibling;
		if (elem.getAttribute('id')=='creators' || elem.getAttribute('id')=='owners')
		{
			setPenalty(id+'-name',1);
			setPenalty(id+'-homepage',1);
			setPenalty(id+'-mailbox',1);
			warningElem.innerHTML = 'At least one organization or individual must be defined.';
		}
		else
		{
			setPenalty(id+'-name',0.99);
			setPenalty(id+'-homepage',0.99);
			setPenalty(id+'-mailbox',0.99);
			warningElem.innerHTML = ''; //'Although not mandatory, you may still define organization or individual.'; REMOVAL REQUESTED BY WIM, 2017.04.18
		}
	}
	updateProgressBar();
}


function validateFiles(warningId,elem,allNames)
{
	elem = document.getElementById(elem); // <div> containing all tables

	var fileNames = elem.getElementsByClassName('artefact-filename');
	var warnings = elem.getElementsByClassName('metadata-warning');

	var id = warningId;
	warningMetadataName = 'file name or path';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	var count = fileNames.length;

	if (count)
	{
		warningElem.innerHTML = '';
		var sectionWarningElem = warningElem;
		var penalty = 0;
		
		// check all files and links
		for (var i=0; i<count; i++)
		{
			var fileName = fileNames[i].innerHTML;
			var linkName = fileNames[i].value;

			warnings[i].innerHTML = '';
			warningElem = warnings[i];
			if (fileName && allNames[fileName]>1)
			{	// check duplicate only if there is a fileName (links have no fileName)
				penalty += 0.9/count;
				warnings[i].innerHTML = 'This file is duplicated. Please, make sure that the full path is unique.';
			}
			else
			if (fileNames[i].file && fileNames[i].file.size>100*1000000) //100MB -- 10^X based
			{
				penalty += 0.5/count;
				warnings[i].innerHTML = 'This file might be quite big. Would it be possible to make it smaller (e.g. by compresing) or to put it on an external host?';
			}
			else
			{
				if (fileName)
				{	// local file
					if (stringPath(fileName,'The path to this local file does not look OK. Are you sure it is correct?'))
					{
						penalty += 0.5/count;
					}
				}
				else
				{
					// remote file
					if (stringEmpty(linkName))
						penalty += 1/count
					else
					if (stringURL(linkName))
						penalty += 0.5/count;
				}
			}
		}
		
		warningElem = sectionWarningElem;
		
		setPenalty(id,penalty);
		if (count>20)
		{
			setPenalty(id,0.1);
			warningElem.innerHTML = 'There are many files in this section. Would you consider packing them in an archive?';
		}
	}
	else
	{	// no files
		if (isMandatory(id))
		{
			setPenalty(id,1);
			warningElem.innerHTML = 'At least one file must be added to this section.';
		}
		else
		{
			setPenalty(id,0.99);
			if (getMax(id)==0)
				warningElem.innerHTML = '';
			else
				warningElem.innerHTML = ''; //'Although not mandatory, it might still be good to add files in this section.'; REMOVAL REQUESTED BY WIM, 2017.04.18
		}
	}
	
	updateProgressBar();
	//console.table(progress);
}

function validateTaxonomySelection(elem,warningId,mandatory)
{
	var str = elem.innerHTML;
	var id = warningId;

	warningMetadataName = 'list';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (mandatory)
	{
		if (stringEmpty(str)) return setPenalty(id,1);
	}
	else
	{
		if (stringEmptyOptional(str)) return setPenalty(id,0.999);
	}
	updateProgressBar();
}

/* REMOVAL REQUESTED BY WIM, 2017.04.18
function validateTaxonomySelections(warningId)
{
	var str=document.getElementById("taxonomy-selected-concepts-gage").innerHTML +
			document.getElementById("taxonomy-selected-concepts-lego").innerHTML +
			document.getElementById("taxonomy-selected-concepts-kntr").innerHTML +
			document.getElementById("taxonomy-selected-concepts-acac").innerHTML;
	var id = warningId;

	warningMetadataName = 'list';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str,'At least one of the two classifications elements below must be selected.')) return setPenalty(id,1);
	updateProgressBar();
}
*/

function validateAllFiles()
{
	// collect all file names (exclude files marked for deletion)
	var names = {};
	var fileNames = document.getElementsByClassName('artefact-filename');
	for (var i=0; i<fileNames.length; i++)
	{
		var elem = fileNames[i];
		while (elem.tagName.toUpperCase()!='TABLE') elem = elem.parentNode; // go up to <table> tag
		if (elem.getAttribute('deleted')=='true') continue;
		
		var fileName = fileNames[i].innerHTML;
		if (!names[fileName]) names[fileName]=0;
		names[fileName]++;
	}
	//console.log(names);
	validateFiles("28-warning",'artefact-sources',names);
	validateFiles("29-warning",'artefact-docs',names);
	validateFiles("30-warning",'artefact-configs',names);
	validateFiles("31-warning",'artefact-tests',names);
	validateFiles("32-warning",'artefact-others',names);
	validateFiles("31media-warning",'artefact-media',names);
}


function validateCodingStyle(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'coding style';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,0.999);
	updateProgressBar();
}


function validateArchitecturalConformance(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'architectural conformance';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmptyOptional(str)) return setPenalty(id,0.999);
	updateProgressBar();
}

/* REMOVAL REQUESTED BY WIM, 2017.04.18
function validateInteroperabilityConformance(elem)
{
	var str = elem.value;
	var id = parseInt(elem.getAttribute('id'))+'-warning';

	warningMetadataName = 'interoperability conformance';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEmpty(str)) return setPenalty(id,1.0);
	updateProgressBar();
}
*/


function validateSoftwareTesting(elem)
{
	var str = '';
	var mainId = parseInt(elem.getAttribute('id'));
	var id = mainId+'-warning';

	for (var i=1; i<=5; i++)
	{
		var subElem = document.getElementById(mainId+'.'+i+'-input');
		if (subElem.getAttribute('checked')=='true') str='x';
	}

	warningMetadataName = 'software testings';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	//if (stringEmptyOptional(str,'It would be good if at least one of the %%% is performed')) return setPenalty(id,1.0);
	if (stringEmptyOptional(str)) return setPenalty(id,0.999);
	updateProgressBar();
}


function validateSelfDeclaration(elem)
{
	var str = '';
	var mainId = parseInt(elem.getAttribute('id'));
	var id = mainId+'-warning';

	for (var i=1; i<=2; i++)
	{
		var subElem = document.getElementById(mainId+'.'+i+'-input');
		if (subElem.getAttribute('checked')=='true') str+='x';
	}

	warningMetadataName = 'self-declaration';
	warningElem = document.getElementById(id);
	if (!warningElem) return;
	
	warningElem.innerHTML = '';
	
	setPenalty(id,0);
	if (stringEqual(str,'','Both options must be agreed on and checked')) return setPenalty(id,1.0);
	if (stringEqual(str,'x','Both options must be agreed on and checked')) return setPenalty(id,1);
	updateProgressBar();
}


function validateSections()
{
	// section 1
	validateAssetTitle(document.getElementById("1-input"));
	validateAssetPromotionalDescription(document.getElementById("41-input"));
	validateAssetTechnicalDescription(document.getElementById("44-input"));
	validateAssetDescription(document.getElementById("2-input"));
	validateAssetLogo(document.getElementById("43-input"));
	validateAssetDate(document.getElementById("datepicker"),'4-warning');
	validateAssetLanguage(document.getElementById("5-input"));
	validateAssetAccessURL(document.getElementById("6-input"));
	// section 2
	/*validateAssetType(document.getElementById("3-input")); REMOVAL REQUESTED BY WIM, 2017.04.18*/
	validateGameEngine(document.getElementById("21-input"));
	validateGamePlatform(document.getElementById("22-input"));
	validateProgrammingLanguage(document.getElementById("23-input"));
	//validateTaxonomySelection(document.getElementById("taxonomy-selected-concepts-gage"),'7-warning') REMOVAL REQUESTED BY WIM, 2017.04.18
	validateTaxonomySelection(document.getElementById("taxonomy-selected-concepts-lego"),'14-warning');
	validateShop(document.getElementById("14shop-input"));
	//validateTaxonomySelection(document.getElementById("taxonomy-selected-concepts-kntr"),'15-warning') REMOVAL REQUESTED BY WIM, 2017.04.18
	validateTaxonomySelection(document.getElementById("taxonomy-selected-concepts-acac"),'16-warning',true);
	//validateTaxonomySelections('34-warning') CHANGE REQUESTED BY WIM, 2017.04.18
	validateAssetKeywords(document.getElementById("8-input"));
	// section 3
	validateAssetVersion(document.getElementById("9-input"));
	validateAssetVersionNotes(document.getElementById("10-input"));
	validateAssetDevelopmentStatus(document.getElementById("11-input"));
	validateCommitURI(document.getElementById("40-input"));
	//validateAssetMaturityLevel(document.getElementById("12-input")); REMOVAL REQUESTED BY WIM, 2017.04.18
	/* NO RELATED ASSETS
	validateRelatedAssetName(document.getElementById("24-input"));
	validateRelatedAssetName(document.getElementById("25-input"));
	validateRelatedAssetVersion(document.getElementById("26-input"),'24-input');
	validateRelatedAssetVersion(document.getElementById("27-input"),'25-input');
	NO RELATED ASSETS */
	//validateAssetCustomMetadata(document.getElementById("13-input")); REMOVAL REQUESTED BY WIM, 2017.04.18
	// section 4
	validateLicenseType(document.getElementById("17-input"));
	validateLicenseURL(document.getElementById("18-input"));
	validateLicenseConditions(document.getElementById("19-input"));
	// section 5
	validateAllContacts("20-warning",'creators');
	//validateAllContacts("20-warning",'publishers'); REMOVAL REQUESTED BY WIM, 2017.04.18
	validateAllContacts("20-warning",'owners');
	// section 6
	/*validateLocalFolder(document.getElementById("33-input")); REMOVAL REQUESTED BY WIM, 2017.04.18*/
	validateAssetDetailedDescription(document.getElementById("42-input"));
	validateAllFiles();
	// section 7
	validateCodingStyle(document.getElementById("35-input")); /* OPTIONAL -- REQUESTED BY DOMINIC, 2018.05.09*/
	validateArchitecturalConformance(document.getElementById("36-input")); /* OPTIONAL -- REQUESTED BY DOMIC 2018.05.09*/
	//validateInteroperabilityConformance(document.getElementById("37-input")); REMOVAL REQUESTED BY WIM, 2017.04.18
	validateSoftwareTesting(document.getElementById("38.1-input"));
	validateSelfDeclaration(document.getElementById("39.1-input"));
}
