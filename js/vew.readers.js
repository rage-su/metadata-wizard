﻿
var compat_list;

function desafe(x)
{
	x = x.trim();
	x = x.split('%26').join('&');
	//x = x.split('&lt;').join('<');
	//x = x.split('&gt;').join('>');
	//x = x.split('&quot;').join('"');
	x = x.split('%3D').join('=');
	x = x.split('%25').join('%'); // must be last

	return x;
}


//https://stackoverflow.com/questions/36810940/array-from-on-the-internet-explorer
if (!Array.from) {
  Array.from = (function () {
    var toStr = Object.prototype.toString;
    var isCallable = function (fn) {
      return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
    };
    var toInteger = function (value) {
      var number = Number(value);
      if (isNaN(number)) { return 0; }
      if (number === 0 || !isFinite(number)) { return number; }
      return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
    };
    var maxSafeInteger = Math.pow(2, 53) - 1;
    var toLength = function (value) {
      var len = toInteger(value);
      return Math.min(Math.max(len, 0), maxSafeInteger);
    };

    // The length property of the from method is 1.
    return function from(arrayLike/*, mapFn, thisArg */) {
      // 1. Let C be the this value.
      var C = this;

      // 2. Let items be ToObject(arrayLike).
      var items = Object(arrayLike);

      // 3. ReturnIfAbrupt(items).
      if (arrayLike == null) {
        throw new TypeError("Array.from requires an array-like object - not null or undefined");
      }

      // 4. If mapfn is undefined, then let mapping be false.
      var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
      var T;
      if (typeof mapFn !== 'undefined') {
        // 5. else
        // 5. a If IsCallable(mapfn) is false, throw a TypeError exception.
        if (!isCallable(mapFn)) {
          throw new TypeError('Array.from: when provided, the second argument must be a function');
        }

        // 5. b. If thisArg was supplied, let T be thisArg; else let T be undefined.
        if (arguments.length > 2) {
          T = arguments[2];
        }
      }

      // 10. Let lenValue be Get(items, "length").
      // 11. Let len be ToLength(lenValue).
      var len = toLength(items.length);

      // 13. If IsConstructor(C) is true, then
      // 13. a. Let A be the result of calling the [[Construct]] internal method of C with an argument list containing the single item len.
      // 14. a. Else, Let A be ArrayCreate(len).
      var A = isCallable(C) ? Object(new C(len)) : new Array(len);

      // 16. Let k be 0.
      var k = 0;
      // 17. Repeat, while k < len… (also steps a - h)
      var kValue;
      while (k < len) {
        kValue = items[k];
        if (mapFn) {
          A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
        } else {
          A[k] = kValue;
        }
        k += 1;
      }
      // 18. Let putStatus be Put(A, "length", len, true).
      A.length = len;
      // 20. Return A.
      return A;
    };
  }());
}

//https://stackoverflow.com/questions/24143604/array-prototype-find-is-undefined
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    enumerable: false,
    configurable: true,
    writable: true,
    value: function(predicate) {
      if (this == null) {
        throw new TypeError('Array.prototype.find called on null or undefined');
      }
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }
      var list = Object(this);
      var length = list.length >>> 0;
      var thisArg = arguments[1];
      var value;

      for (var i = 0; i < length; i++) {
        if (i in list) {
          value = list[i];
          if (predicate.call(thisArg, value, i, list)) {
            return value;
          }
        }
      }
      return undefined;
    }
  });
}

function findFirstElementChild(node)
{	// https://github.com/w3c/respec/commit/64c43aa583bcb222b858c8b41b6e9d516935c8df

	if (!node.hasChildNodes())
		return null;

	if (node.firstElementChild)
		return node.firstElementChild;

	if (node.childNodes[0].nodeType === Node.ELEMENT_NODE)
		return node.childNodes[0];

	return Array
		.from(node.childNodes)
		.find(function(node){
			return node.nodeType === Node.ELEMENT_NODE;
		});
}
 
 

function readMetadata(metadata)
{
	var meta = (new DOMParser()).parseFromString(metadata,'text/xml');
	
	var asset = findFirstElementChild(meta);
	readAsset( asset );
}

function readAsset( asset )
{
	// scan asset elements
	for (var elem=findFirstElementChild(asset); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'title': readAssetTitle(elem); break;
			case 'description': readAssetDescription(elem); break;
			case 'type': /*readAssetType(elem); REMOVAL REQUESTED BY WIM, 2017.04.18*/break;
			case 'date': readAssetDate(elem); break;
			case 'language': readAssetLanguage(elem); break;
			case 'creator': readAssetCreator(elem,'creators'); break;
			case 'publisher': /*readAssetCreator(elem,'publishers');REMOVAL REQUESTED BY WIM, 2017.04.18*/ break;
			case 'owner': readAssetCreator(elem,'owners'); break;
			case 'keyword': readAssetKeywords(elem); break;
			case 'versionInfo': readAssetVersionInfo(elem); break;
			case 'versionNotes': readAssetVersionNotes(elem); break;
			case 'status': readAssetStatus(elem); break;
			//case 'maturityLevel': /*readAssetMaturityLevel(elem); REMOVAL REQUESTED BY WIM, 2017.04.18*/break;
			case 'maturityLevel': readAssetMaturityLevelPublish(elem); /*ADDING REQUESTED BY DOMINIQ, 2017.11.26*/break;
			case 'accessURL': readAssetAccessURL(elem); break;
			case 'Classification': readAssetClassification(elem); break;
			case 'Solution': readAssetSolution(elem); break;
			case 'Usage': readArtefactSection(elem,'Usage'); break;
			case 'License': readAssetLicense(elem); break;
			case 'CustomMetadata': readAssetCustomMetadata(elem); break;
			/* NO RELATED ASSETS
			case 'RelatedAsset': readRelatedAsset(elem); break;
			NO RELATED ASSETS */
		}
	}
}


function getElementsByTagName(dom,ns,tag)
{
	var tags = dom.getElementsByTagName(tag);
	if (!tags.length)
		tags = dom.getElementsByTagName(ns+':'+tag);
	return tags;
}

function readAssetTitle( dom )
{
	document.getElementById('1-input').innerHTML = desafe(dom.innerHTML||dom.textContent);
	document.getElementById('1001-input').innerHTML = desafe(dom.innerHTML||dom.textContent);
}


function readAssetDescription( dom )
{
	addString('2-input', desafe(dom.innerHTML||dom.textContent));
}


function readAssetLanguage( dom )
{
	addString('5-input',desafe(dom.innerHTML||dom.textContent)||'English'); // ENGLISH REQUESTED BY WIM, 2017.04.18

}


function readAssetDate( dom )
{
	addString('4-input',dom.innerHTML||dom.textContent);
}


function readAssetVersionInfo( dom )
{
	addString('9-input',dom.innerHTML||dom.textContent);
}


function readAssetVersionNotes( dom )
{
	addString('10-input',marked(desafe(dom.innerHTML||dom.textContent)));
}


function readAssetCustomMetadata( data )
{
	// scan elements
	var name,value;
	for (var elem=findFirstElementChild(data); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'name': name = desafe(elem.innerHTML||elem.textContent); break;
			case 'value': value = desafe(elem.innerHTML||elem.textContent); break;
		}
	}

	var codingStyles = ['','Individual programmer style','Coding conforms to explicit style guide','Third-party validation of code against style guide'];
	var conformances = ['','Not checked','Checked, some conformance','Checked, fully conforms','The component is server-side'];
	var softwareTesting = ['Unit tests','Integration tests with RAGE’s Component Manager','Integration tests within a game platform','End-to-end tests','Performance tests'];
	
	switch (name)
	{
		case '%sys%metadata version':
			if (value) addString('metadata-version',value);
			break;
		case '%sys%coding style':
			if (value) addString('coding-style',codingStyles[value]);
			break;
		case '%sys%architectural conformance':
			if (value) addString('architectural-conformance',conformances[value]);
			break;
		/* REMOVAL REQUESTED BY WIM, 2017.04.18	
		case '%sys%interoperability conformance':
			if (value) addString('interoperability-conformance',conformances[value]);
			break;
		*/
		case '%sys%software testing':
			for (var i=0; i<5; i++)
				if (value[i]=='1') addString('software-testing',softwareTesting[i]);
			break;
		case '%sys%self declaration':
			if (value='11')
				setString('self-declaration','Signed');
			else
				setString('self-declaration','Not signed');
			break;
		case '%sys%asset completeness':
			if (value) setString('asset-completeness',value);
			break;
		case '%sys%asset quality':
			if (value) setString('asset-quality',value);
			break;
		case '%sys%commit uri':
			if (value)
			{
				value = '<a class="download-artefact" href="'+value+'" target="_blank">'+value+'</a>';
				setString('40-input',value);
			}
			break;
		case '%sys%promotional description':
			if (value) setString('41-input',marked(value));
			break;
		case '%sys%technical description':
			if (value) setString('44-input',marked(value));
			break;
		case '%sys%detailed description':
			if (value) setString('42-input',marked(value));
			break;
		default:
			//appendString('13-input','<h3>'+name+': <span class="metadata-text">'+value+'</span></h3>'); REMOVAL REQUESTED BY WIM, 2017.04.18
			break;
	}
}


function readAssetStatus( dom )
{
	addTaxon('11-input','development-status',dom.getAttribute('rdf:resource'));
}


/* REMOVAL REQUESTED BY WIM, 2017.04.18
function readAssetMaturityLevel( dom )
{
	addString('12-input',dom.innerHTML||dom.textContent);
}
*/


/* ADDIN REQUESTED BY DOMINIQ, 2017.11.26 */
function readAssetMaturityLevelPublish( dom )
{
	var value = dom.innerHTML||dom.textContent;
	if (value=='' || value=='1')
		addString('asset-published','Published to the RAGE portal');
	else
		addString('asset-published','Not published to the RAGE portal');
}



function taxon( taxonomyName, id )
{
	if (id=='')
		return '';
		
	if (taxonomies[taxonomyName][id])
		return taxonomies[taxonomyName][id];
		
	return id+' (!)';
}

function addString(targetId,string)
{
	var elem = document.getElementById(targetId)
	if (elem.innerHTML) elem.innerHTML += ', ';
	elem.innerHTML += string;
}

function setString(targetId,string)
{
	var elem = document.getElementById(targetId)
	elem.innerHTML = string;
}

function appendString(targetId,string)
{
	var elem = document.getElementById(targetId)
	elem.innerHTML += string;
}

function addTaxon(targetId,taxonomyName,conceptName)
{
	addString(targetId,taxon(taxonomyName,conceptName));
}

/* REMOVAL REQUESTED BY WIM, 2017.04.18
function readAssetType( dom )
{
	document.getElementById('3-input').innerHTML = taxon('http://rageproject.eu/taxonomy/asset-type#asset-type',dom.getAttribute('rdf:resource'));
}
*/


function readAssetKeywords( dom )
{
	addString('8-input',desafe(dom.innerHTML||dom.textContent));
}


function readAssetAccessURL( dom )
{
	addString('6-input',dom.getAttribute('rdf:resource'));
}


function readAssetCreator( dom, id )
{
	var s = '';
	s += '<div style="margin-left: 1em" class="metadata-text">&#x25CF;&nbsp;&nbsp; '+desafe(getElementsByTagName(dom,'foaf','name')[0].innerHTML || getElementsByTagName(dom,'foaf','name')[0].textContent);
	
	var data = [];
	if (getElementsByTagName(dom,'foaf','mbox')[0])
		var mbox = getElementsByTagName(dom,'foaf','mbox')[0].getAttribute('rdf:resource').replace('mailto:','');
	if (getElementsByTagName(dom,'foaf','homepage')[0])
		var page = getElementsByTagName(dom,'foaf','homepage')[0].getAttribute('rdf:resource');
	if (mbox||page)
	{
		s += ' [<small>';
		if (mbox&&page)
			s += mbox+', '+page;
		else
			s += mbox||page;
		s += '</small>]';
	}
	s += '</div>';
	appendString(id,s);
}


function readAssetLicense( license )
{
	// scan elements
	for (var elem=findFirstElementChild(license); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'description': readLicenseDescription(elem); break;
			case 'accessURL': readLicenseAccessURL(elem); break;
			case 'type': readLicenseType(elem); break;
		}
	}
}


function readLicenseDescription( dom )
{
	addString('19-input',desafe((dom.innerHTML||dom.textContent).replace('MEW%N/A?WEM','')));
}


function readLicenseAccessURL( dom )
{
	var value = dom.getAttribute('rdf:resource');
	value = '<a class="download-artefact" href="'+value+'" target="_blank">'+value+'</a>';
	addString('18-input',value);
}


function readLicenseType( dom )
{
	addTaxon('17-input','http://rageproject.eu/taxonomy/licns-type#licns-type',dom.getAttribute('rdf:resource'));
}


var classifications;
function readAssetClassification( classification )
{
	// scan elements and collect used taxonomies
	classifications = {};
	for (var elem=findFirstElementChild(classification); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'description': /*addCompatList('Ignored classification description');*/ break; // ignore silently
			case 'Context': readClassificationContext(elem); break;
			//default: { --ignore silently
			//	addCompatList('Unknown classification tags');
			//	console.error('Unknown and unsupported classification tag '+elem.localName);
			//	console.dir(elem);
			//}
		}
	}
	
	// process classifications
	for (var name in classifications)
	{
		//console.log(name);
		switch (name)
		{
			/* REMOVAL REQUESTED BY WIM, 2017.04.18
			case 'http://rageproject.eu/taxonomy/game-genre#game-genre':
				for (var i=0; i<classifications[name].length; i++)
					addTaxon('7-input',name,classifications[name][i]);
				break;
			*/
			case 'http://rageproject.eu/taxonomy/learnng-goals#learnng-goals':
				for (var i=0; i<classifications[name].length; i++)
					addTaxon('14-input',name,classifications[name][i]);
				break;
			/* REMOVAL REQUESTED BY WIM, 2017.04.18
			case 'http://rageproject.eu/taxonomy/knowldg-transfer#knowldg-transfer':
				for (var i=0; i<classifications[name].length; i++)
					addTaxon('15-input',name,classifications[name][i]);
				break;
			*/
			case 'http://rageproject.eu/taxonomy/acm-ccs-applied#acm-ccs-applied':
				for (var i=0; i<classifications[name].length; i++)
					addTaxon('16-input',name,classifications[name][i]);
				break;
			case 'http://rage.deploy2.ftk.de/taxonomy/SHOP#59bfe60d4f26c':
				for (var i=0; i<classifications[name].length; i++)
					addTaxon('14shop-input',name,classifications[name][i]);
				break;
		}
	}
}



function readClassificationContext( context )
{
	// ignore attributes
	
	// scan elements and collect used taxonomies
	var taxonomy;
	for (var elem=findFirstElementChild(context); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'themeTaxonomy':
					taxonomy = elem.getAttribute('rdf:resource');
					classifications[taxonomy] = classifications[taxonomy]||[];
					break;
			case 'theme':
					var concept = elem.getAttribute('rdf:resource');
					if (classifications[taxonomy].indexOf(concept)<0)
						classifications[taxonomy].push(concept);
					break;
		}
	}
}


function readArtefactSection( usage, sectionId )
{
	// scan usage elements
	for (var elem=findFirstElementChild(usage); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'Artefact': readArtefact(elem,sectionId); break;
			case 'progLanguage': readProgLanguage(elem); break;
			case 'gamePlatform': readGamePlatform(elem); break;
			case 'gameEngine': readGameEngine(elem); break;
		}
	}
}



// mapping between old model and mew model -> see mew.producers.js
function readArtefact( artefact,sectionId )
{
	var attrName = artefact.getAttribute('name');
	
	// collect artefact metadata
	var title,accessURL,type,reference,format,date;
	for (var elem=findFirstElementChild(artefact); elem; elem=elem.nextElementSibling)
	{
		//console.log('>>',elem.localName);
		switch (elem.localName)
		{
			case 'title': title = desafe(elem.innerHTML||elem.textContent); break;
			case 'type': type = desafe(elem.getAttribute('rdf:resource')); break;
			case 'accessURL': accessURL = desafe(elem.getAttribute('rdf:resource')); break;
			case 'reference': reference = desafe(elem.innerHTML||elem.textContent); break;
			case 'format': format = elem.innerHTML||elem.textContent||elem.getAttribute('rdf:resource').split('#')[1]||''; break;
			case 'date': date = elem.innerHTML||elem.textContent; break;
		}
	}

	// if it is asset logo, process it now
	if (reference && accessURL)
	if (reference.indexOf('_rage_logo_/')==0)
	{
		var logoElem = document.getElementById('43-input');
		logoElem.src = accessURL;
		logoElem.style.display = 'inline';
	}
	
	// decide MEW section based on type and metadata section
//console.log('');	
//console.log('type=',type);	
//console.log('section=',sectionId);	
	var mewId;
	if (reference=='_rage_youtube_')
		mewId = 'artefact-youtube';
	else if (reference && reference.indexOf('_rage_snapshot_/')==0)
		mewId = 'artefact-snapshots';
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#source-code'
		|| (!type && sectionId=='Implementation')) mewId = 'artefact-sources';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#documentation'
		|| type=='http://rageproject.eu/taxonomy/artefct-type#tutorial'
		|| (!type && sectionId=='Design')) mewId = 'artefact-docs';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#demonstration'
		|| type=='http://rageproject.eu/taxonomy/artefct-type#setup-config'
		|| (!type && sectionId=='Usage')) mewId = 'artefact-configs';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#test'
		|| (!type && sectionId=='Tests')) mewId = 'artefact-tests';
		
	else if ( type=='http://rageproject.eu/taxonomy/artefct-type#data'
		|| (!type && sectionId=='Requirements')) mewId = 'artefact-others';
	else
	{
		return;
	}
//console.log('mewId=',mewId);	

/*
console.log('=================');
console.log('title',title);
console.log('accessURL',accessURL);
console.log('type',type);
console.log('reference',reference);
console.log('format',format);
console.log('date',date);
console.log('=================');
*/
//console.log('=>',reference);	
	if (reference && reference!='_rage_youtube_')
	{
		reference = reference.replace('_rage_snapshot_/','');
		var s = '<div style="margin-left: 1em" class="metadata-text">&#x25CF;&nbsp;&nbsp; '
		if (accessURL)
			s += ' <a class="download-artefact" href="'+accessURL+'">'+reference+'</a>';
		else
			s += reference;

		if (date||format)
		{
			s += ' [<small>';
			if (date&&format)
				s += date+', '+format;
			else
				s += date||format;
			s += '</small>]';
		}

		if (accessURL)
			s += '</div>';
			
		appendString(mewId,s);
	}
	else
	{
		var s = '<div style="margin-left: 1em" class="metadata-text">&#x25CF;&nbsp;&nbsp; ';
		s += '<a class="download-artefact" href="'+(accessURL||title||attrName)+'" target="_blank">'+(accessURL||title||attrName)+'</a>';
		s += '</div>';
		appendString(mewId,s);
	}
}


function readAssetSolution( solution )
{
	// scan elements
	for (var elem=findFirstElementChild(solution); elem; elem=elem.nextElementSibling)
	{
		//console.log(elem.localName);
		switch (elem.localName)
		{
			case 'Design': readArtefactSection(elem,'Design'); break;
			case 'Implementation': readArtefactSection(elem,'Implementation'); break;
			case 'Tests': readArtefactSection(elem,'Tests'); break;
			case 'Requirements': readArtefactSection(elem,'Requirements'); break;
		}
	}
}


function readProgLanguage( dom )
{
	addTaxon('23-input','http://rageproject.eu/taxonomy/progrmmng-langg#progrmmng-langg',dom.getAttribute('rdf:resource'));
}


function readGameEngine( dom )
{
	addTaxon('21-input','http://rageproject.eu/taxonomy/game-engine#game-engine',dom.getAttribute('rdf:resource'));
}


function readGamePlatform( dom )
{
	addTaxon('22-input','http://rageproject.eu/taxonomy/game-platfrm#game-platfrm',dom.getAttribute('rdf:resource'));
}


/* NO RELATED ASSETS
function readRelatedAsset( relation )
{
	var name,minVersion,maxVersion,relationType;
	
	// read attributes
	for (var i=0; i<relation.attributes.length; i++)
	{
		var attr = relation.attributes[i];
		switch (attr.name)
		{
			case 'name': name = attr.nodeValue; break;
			case 'minVersion': minVersion = attr.nodeValue; break;
			case 'maxVersion': maxVersion = attr.nodeValue; break;
			// ignore all others
		}
	}
	
	// scan elements
	for (var elem=findFirstElementChild(relation); elem; elem=elem.nextElementSibling)
		if (elem.localName=='relationType') relationType = elem.innerHTML;
	
	switch (relationType)
	{
		case 'prevVersion':
			addString('24-input',name);
			addString('26-input',maxVersion||minVersion);
			break;
		case 'nextVersion':
			addString('25-input',name);
			addString('27-input',maxVersion||minVersion);
			break;
	}
}
NO RELATED ASSETS */

function hideUnusedElements()
{
	var parents = ["1001-input","2-input","3-input","4-input","5-input","6-input","7-input","8-input","9-input","10-input","11-input","40-input","41-input","42-input","44-input","12-input","14-input","14shop-input","15-input","16-input","21-input","22-input","23-input",/*NO RELATED ASSETS "24-input","25-input","26-input","27-input", NO RELATED ASSETS*/"18-input","19-input","17-input","creators",/*"publishers",REMOVAL REQUESTED BY WIM, 2017.04.18*/"owners","artefact-sources","artefact-docs","artefact-configs","artefact-tests","artefact-others","coding-style","architectural-conformance",/*"interoperability-conformance", REMOVAL REQUESTED BY WIM, 2017.04.18*/"software-testing","self-declaration","asset-completeness","asset-quality",'artefact-snapshots','artefact-youtube','asset-published'];

	for (var i=0; i<parents.length; i++)
	{
		var elem = document.getElementById(parents[i]);
		if (!elem) continue;
		if ((elem.innerHTML||elem.textContent).trim()) continue;
		if ('acem-rypy' in options)
			elem.parentNode.style.color = 'Tomato';
		else
			elem.parentNode.style.display = 'none';
	}
	
	function allHidden(section,list)
	{
		for (var i=0; i<list.length; i++)
		{
			var elem = document.getElementById(list[i]);
			if (!elem) continue;
			if ((elem.innerHTML||elem.textContent).trim()) return;
		}
		var elem = document.getElementById(section);
		if ('acem-rypy' in options)
			elem.style.color = 'Tomato';
		else
			elem.style.display = 'none';
	}

	allHidden("h2-about",["1001-input","2-input","4-input","5-input","6-input","41-input","44-input"]);
	allHidden("h2-classification",["3-input","21-input","22-input","23-input","7-input","14-input","15-input","16-input","8-input"]);
	allHidden("h2-status",["9-input","10-input","11-input","12-input"/*NO RELATED ASSETS ,"24-input","26-input","25-input","27-input" NO RELATED ASSETS*/,"40-input"]);
	allHidden("h2-license",["17-input","18-input","19-input"]);
	allHidden("h2-contacts",["creators",/*"publishers",REMOVAL REQUESTED BY WIM, 2017.04.18*/"owners"]);
	allHidden("h2-resources",["42-input","artefact-sources","artefact-docs","artefact-configs","artefact-tests","artefact-others"]);
	allHidden("h2-quality",["coding-style","architectural-conformance",/*"interoperability-conformance", REMOVAL REQUESTED BY WIM, 2017.04.18*/"software-testing","self-declaration","asset-completeness","asset-quality"]);
}
