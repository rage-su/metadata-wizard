/*
  * api.js
	API for RAGE taxonomy services and RAGE asset services.
	
  * Copyright 2015-2016 Sofia University
  
	Licensed under the Apache License, Version 2.0 (the "License");
	you may not use this file except in compliance with the License.
	This project has received funding from the European Union’s Horizon
	2020 research and innovation programme under grant agreement No 644187.
	You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

	Unless required by applicable law or agreed to in writing, software
	distributed under the License is distributed on an "AS IS" BASIS,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
	See the License for the specific language governing permissions and
	limitations under the License.
	
	========================================================
*/

var RAGE_SERVER = "https://repository.gamecomponents.eu";

var rage = (function(opt){

	var SERVER = RAGE_SERVER;
	var configTaxon = {
		baseUrl: SERVER+"/taxonomies",
		accept: "application/ld+json",
		contentType: "application/ld+json;charset=utf-8"
	};
	
	var configAsset = {
		baseUrl: SERVER+"/assets",
		accept: "application/ld+json",
		contentType: "application/ld+json;charset=utf-8",
		contentTypeZIP: "application/zip",
		contentTypeXML: "text/xml"
	};
	
	var configSearch = {
		baseUrl: SERVER+"/assets/search",
		accept: "application/ld+json",
		contentType: "application/ld+json;charset=utf-8"
	};
	
	
	// ASSET SERVICES
	
	function ping(opt) {
		var url = configAsset.baseUrl+'/ping',
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
			//hideLoader();
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("GET", url, true);
		xhr.timeout = 6000; // timeout 6 seconds
		xhr.setRequestHeader("Accept", configAsset.accept);
		xhr.send();		
	}

	function getAssetsList(opt) {
		var url = configAsset.baseUrl+'?limit='+opt.limit+'&offset='+opt.offset,
			xhr = new XMLHttpRequest();

		//showLoader();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
			//hideLoader();
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("GET", url, true);
		xhr.timeout = 6000; // timeout 6 seconds
		xhr.setRequestHeader("Accept", configAsset.accept);
		xhr.send();		
	}

	function deleteAsset(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId,
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 204) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("DELETE", url, true);
		xhr.timeout = 12000; // timeout 12 seconds
		xhr.send();		
	}

	function downloadAsset(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId,
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("HEAD", url, true); // just test whether the ZIP file exists
		//xhr.timeout = 6000; // timeout 6 seconds -- no timeout
		xhr.setRequestHeader("Accept", configAsset.contentTypeZIP);
		xhr.send();		
	}

	function cloneAsset(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId+'/copy',
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 201) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("POST", url, true);
		//xhr.timeout = 6000; // timeout 6 seconds -- no timeout
		//xhr.setRequestHeader("Accept", configAsset.accept);
		xhr.send();		
	}

	function searchAssetsList(opt) {
		var url = configSearch.baseUrl+'?q='+encodeURIComponent(opt.search)+'&limit='+opt.limit+'&offset='+opt.offset,
			xhr = new XMLHttpRequest();

		showLoader();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
				  hideLoader();
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("GET", url, true);
		xhr.timeout = 6000; // timeout 6 seconds
		xhr.setRequestHeader("Accept", configSearch.accept);
		xhr.send();		
	}

	function newAsset(opt) {
		var url = configAsset.baseUrl+'/new',
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 201) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("POST", url, true);
		xhr.timeout = 6000; // timeout 6 seconds
		//xhr.setRequestHeader("Content-Type", configAsset.contentType);
		xhr.send();		
	}	

	function uploadAsset(opt) {
		var url = configAsset.baseUrl+'/upload',
			xhr = new XMLHttpRequest(),
			fd = new FormData();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 201) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			

		xhr.onprogress = opt.progress;
		xhr.upload.onprogress = opt.progress;
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("POST", url);
		fd.append("file", opt.file, opt.file.name);

		xhr.send(fd);
	}	
	
	function uploadAssetMetadata(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId+'/metadata?publish='+opt.publish,
			xhr = new XMLHttpRequest();
			fd = new FormData();

		//showLoader();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 201) {
				  	opt.success(xhr.responseText);
				  } else {
					console.dir(xhr);
					opt.error(xhr.statusText);
				  }
				  //hideLoader();
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("POST", url, true);
		xhr.withCredentials = true;
		//alert(opt.metadata);
		//var blob = new Blob([opt.metadata], { type: "text/xml"});
		fd.append("metadata", opt.metadata);
		//xhr.timeout = 6000; // timeout 6 seconds - no timeout
		//xhr.setRequestHeader("Content-Type", configAsset.contentTypeXML);
		xhr.send(fd);
		//xhr.send(opt.metadata);
	}	
	
	function getAssetMetadata(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId+'/metadata',
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("GET", url/*, true*/);
		xhr.timeout = 6000; // timeout 6 seconds
		//xhr.setRequestHeader("Accept", configAsset.contentTypeXML);
		xhr.send();		
	}

	function getArtefactMetadata(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId+'/artefacts/'+opt.artefactId+'/metadata',
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("GET", url, true);
		xhr.timeout = 6000; // timeout 6 seconds
		xhr.setRequestHeader("Accept", configAsset.contentTypeXML);
		xhr.send();		
	}
	
	function downloadArtefact(opt) {
		var url = configAsset.baseUrl+opt.url,
			xhr = new XMLHttpRequest();
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("HEAD", url, true);// just test whether the ZIP file exists
		//xhr.timeout = 6000; // timeout 6 seconds -- no timeout
		//xhr.setRequestHeader("Accept", configAsset.contentTypeZIP); -- could be any type, not just ZIP
		xhr.send();		
	}

	function deleteArtefact(opt) {
		var url = configAsset.baseUrl+opt.url,
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 204) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("DELETE", url, true);
		xhr.timeout = 6000; // timeout 6 seconds
		xhr.send();		
	}

	function uploadArtefact(opt) {
		var url = configAsset.baseUrl+'/'+opt.assetId+'/artefacts?section='+encodeURIComponent(opt.section)+'&path='+encodeURIComponent(opt.path),
			xhr = new XMLHttpRequest();
			fd = new FormData();
			
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 201) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;

		xhr.open("POST", url, true);
		fd.append("file", opt.file);
		//xhr.timeout = 6000; // timeout 6 seconds - no timeout
		xhr.send(fd);
	}	
	
	function uploadArtefactMetadata(opt) {
		var url = configAsset.baseUrl+"/"+opt.resource+'/metadata',
			xhr = new XMLHttpRequest();
			fd = new FormData();

		showLoader();
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 201) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
				  hideLoader();
			}
		}			
		xhr.ontimeout = opt.timeout||opt.error;
		
		xhr.open("POST", url, true);
		//alert(opt.metadata);
		//var blob = new Blob([opt.metadata], { type: "text/xml"});
		fd.append("metadata", opt.metadata);
		//xhr.timeout = 6000; // timeout 6 seconds - no timeout
		//xhr.setRequestHeader("Content-Type", configAsset.contentTypeXML);
		xhr.send(fd);
		//xhr.send(opt.metadata);
	}	
	

	
	// TAXONOMY SERVICES
	
	function list(opt) {
		var url = configTaxon.baseUrl,
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
					if(typeof(Storage) !== "undefined")
						localStorage.setItem(url,xhr.responseText);
					//console.log(xhr.responseText);
				  	opt.success(xhr.responseText);
				  } else {
					if( typeof(Storage) !== "undefined")
					{
						var oldResponseText = localStorage.getItem(url);
						if (oldResponseText!=null)
						{
							if (opt.cache) opt.cache(url);
							opt.success(oldResponseText);
							return;
						}
					}
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();		
	}

	function get(opt) {
		var url = configTaxon.baseUrl.concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();
console.log('RAGE.GET('+url+')');
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();		
	}
	
	function getByURI(opt) {
		var url = configTaxon.baseUrl.concat("/search", "?themeTaxonomy=", opt.themeTaxonomy),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
//console.log(url,xhr.readyState,xhr.status);
				  if ((xhr.status == 200) && xhr.responseText!='[ ]') {
					//if(typeof(Storage) !== "undefined")
					//	localStorage.setItem(url,xhr.responseText);
				  	opt.success(xhr.responseText);
				  } else {
					//if( typeof(Storage) !== "undefined")
					//{
					//	var oldResponseText = localStorage.getItem(url);
					//	if (oldResponseText!=null && oldResponseText!='[ ]')
					//	{
					//		if (opt.cache) opt.cache(url);
					//		opt.success(oldResponseText);
					//		return;
					//	}
					//}
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("GET", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();		
	}	

	function post(opt) {
		var url = configTaxon.baseUrl.concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("POST", url, true);
		xhr.setRequestHeader("Content-Type", configTaxon.contentType);
		xhr.send(opt.data);		
	}


	function erase(opt) {
		var url = configTaxon.baseUrl.concat("/", opt.taxonId),
			xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4) {
				  if (xhr.status == 200) {
				  	opt.success(xhr.responseText);
				  } else {
					opt.error(xhr.statusText);
				  }
			}
		}			
		xhr.open("DELETE", url, true);
		xhr.setRequestHeader("Accept", configTaxon.accept);
		xhr.send();			
	}

	/*	========================================================
		showLoader()

		Command. Shows loading animation in the message window.
		========================================================
	*/
	function showLoader(text)
	{
		//console.error('showLoader','['+text+']');
		document.getElementById('whitening').style.display = 'block';
		document.getElementById('loader').style.display = 'block';
		if (text)
		{
			document.getElementById('loader-info').style.display = 'block';
			document.getElementById('loader-info').innerHTML = text;
		}
	}

	/*	========================================================
		hideLoader()

		Command. Hides the loading animaton.
		========================================================
	*/
	function hideLoader()
	{
		//console.error('hideLoader');
		document.getElementById('whitening').style.display = 'none';
		document.getElementById('loader').style.display = 'none';
		document.getElementById('loader-info').style.display = 'none';
	}	

	return {
		SERVER: SERVER,
		
		// General services
		ping: ping,
		
		// Asset services
		getAssetMetadata: getAssetMetadata,
		getAssetsList: getAssetsList,
		deleteAsset: deleteAsset,
		downloadAsset: downloadAsset,
		cloneAsset: cloneAsset,
		searchAssetsList: searchAssetsList,
		newAsset: newAsset,
		uploadAsset: uploadAsset,
		uploadAssetMetadata: uploadAssetMetadata,

		// Artefact services
		getArtefactMetadata: getArtefactMetadata,
		downloadArtefact: downloadArtefact,
		deleteArtefact: deleteArtefact,
		uploadArtefact: uploadArtefact,
		uploadArtefactMetadata: uploadArtefactMetadata,
		
		// Taxonomy services
		list: list,		
		get: get,
		getByURI: getByURI,
		post: post,
		erase: erase,
		
		// Misc
		showLoader: showLoader,
		hideLoader: hideLoader
	}

}());

/*rage.list( {
	success:function(a){console.log('s1',a);},
	error:function(a){console.log('e1');}
} );

rage.getByURI( {
	themeTaxonomy: encodeURIComponent('https://repository.gamecomponents.eu/taxonomies/AssetStatus'),
	success:function(a){console.log('s1',a);},
	error:function(a){console.log('e1',a);}
} );
rage.getByURI( {
	themeTaxonomy: encodeURIComponent('http://purl.org/adms/status/1.0/'),
	success:function(a){console.log('s2',a);},
	error:function(a){console.log('e2',a);}
} );
rage.getByURI( {
	themeTaxonomy: encodeURIComponent('AssetStatus'),
	success:function(a){console.log('s3',a);},
	error:function(a){console.log('e3',a);}
} );

rage.get( {
	taxonId:encodeURIComponent('AssetStatus'),
	success:function(a){console.log('s2',a);},
	error:function(a){console.log('e2');}
} );
*/
